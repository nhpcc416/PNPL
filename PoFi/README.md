#PoFi

We run PoFi's virtual AP on Netgear R6100.

We build an OpenWrt firmware with POFSwitch 1.4.015 and the ath9k patch for Netgear R6100. Note that the patch file we used here is borrowed from Odin-Wi5 project.

#Run the agent

1.1 install PofSwitch：
```
opkg install PofSwitch_1_ar71xx.ipk
```
1.2 config interface：	
```
iw phy phy0 interface add mon0 type monitor
iw dev wlan0 del
ifconfig mon0 up
iw mon0 set bitrates legacy-2.4 54
iw mon0 info
```
1.3 run pofswitch
```
pofswitch -a mon0 -a eth1 -i 172.16.1.172
>enable_color
>enable_promisc
tcpdump -i mon0 -w mon.cap
```

#Run the controller
We just have to follow the instructions of PNPL to install and run PoFi controller


this is Master branch.

