header ipv4;
header icn;

header ethernet {
	fields {
		mac_dst : 48;
		mac_src : 48;
		ethertype : 16;
	}
	next select (ethertype) {
		case 0x0800: ipv4;
		case 0x6666: icn;
	}
}

header ipv4 {
	fields {
		__ver : 4;
		ihl : 4;
		__tos : 8;
		__len : 16;
		__id : 16;
		__flag : 3;
		__off : 13;
		ttl : 8;
		ip_type : 8;
		sum : 16;
		ip_src : 32;
		ip_dst : 32;
		opt : *;
	}
	length : ihl << 2;
	checksum : sum;
}

header icn {
	fields {
	    len : 8;
		hash : 64;
        selectors : 16;
		lifetime : 8;
		name : *;
	}
	length : len << 2;
}

start ethernet;

