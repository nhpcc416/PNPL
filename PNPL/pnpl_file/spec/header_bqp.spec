header ipv4;
header ipv6;
header newProto;

header ethernet {
	fields {
		dl_dst : 48;
		dl_src : 48;
		dl_type : 16;
	}
	next select (dl_type) {
		case 0x1234: newProto;
		case 0x0800: ipv4;
		case 0x86dd: ipv6;
	}
}

header newProto {
	fields {
		field_1 : 16;
		field_2 : 8;
		field_3 : 8;
		field_4 : 16;
	}
}

header ipv4 {
	fields {
		__ver : 4;
		ihl : 4;
		__tos : 8;
		__len : 16;
		__id : 16;
		__flag : 3;
		__off : 13;
		ttl : 8;
		nw_proto : 8;
		sum : 16;
		nw_src : 32;
		nw_dst : 32;
	}
}

header ipv6 {
	fields {
		__ver : 4;                  //version  ipv6:6
		__tc : 8;                   //traffic class
        __fl: 20;               	//flow label
        pl: 16;                		//payload length
        eip_type: 8;           		//next header type
        hl: 8;                 		//hop limit
        __eip_src: 128;
        __eip_dst: 128;
	}
}

start ethernet;
