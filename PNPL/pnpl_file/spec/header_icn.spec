header icn;

header ethernet {
	fields {
		dl_dst : 48;
		dl_src : 48;
		dl_type : 16;
	}
	next select (dl_type) {
		case 0x6666: icn;
    }
}

header icn {
	fields {
	    len : 8;
		hash : 64;
        selectors : 16;
		lifetime : 8;
		name : *;
	}
	length : len << 2;
}

start ethernet;

