
header ipv6;

header ethernet {
	fields {
		dl_dst : 48;
		dl_src : 48;
		dl_type : 16;
	}
	next select (dl_type) {
		case 0x86dd: ipv6;
	}
}

header ipv6 {
	fields {
		__ver : 4;                     //version  ipv6:6
		__tc : 8;                       //traffic class
        __fl: 20;               //flow label
        pl: 16;                //payload length
        eip_type: 8;           //next header type
        hl: 8;                 //hop limit
        __eip_src: 128;
        __eip_dst: 128;
        exhd: *;               //extended header:length is varible
	}
}

start ethernet;




