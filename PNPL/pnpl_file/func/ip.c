#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

void header_adding(struct packet *pkt,uint8_t *len, uint64_t *hash, uint16_t *selectors, uint8_t *lifetime, uint64_t *name){
	add_field(pkt, 8*14, 8*1, value_from_8(*len));
	add_field(pkt, 8*15, 8*8, value_from_64(*hash));
	add_field(pkt, 8*23, 8*2, value_from_16(*selectors));
	add_field(pkt, 8*25, 8*1, value_from_8(*lifetime));
	add_field(pkt, 8*26, 8*8, value_from_64(*name));
	mod_packet(pkt, "dl_type", value_from_16(0x6666));
	return;
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

    if(dpid==1){
	    if(dl_type == 0x0800){
	    	//update_checksum(pkt);
		    pull_header(pkt);	// goto ip
		    uint32_t nw_dst = value_to_32(read_packet(pkt, "nw_dst"));
		    push_header(pkt);
	        if(nw_dst==0x0b000002){
	        	//decrease_ttl(pkt);
	        	return forward(me,in_port,2);
	        }
	        else if(nw_dst==0x0b000003){
	        	//decrease_ttl(pkt);
		        return forward(me,in_port,3);
	        }
	    }
    }
    if(dpid==2){
    	if(dl_type == 0x0800){
    		//update_checksum(pkt);
    		uint8_t len=0x05;
    		uint64_t hash=0x1234567890abcdee;
    		uint16_t selectors=0;
    		uint8_t lifetime=0;
    		uint64_t name=0;
    		header_adding(pkt,&len, &hash, &selectors, &lifetime, &name);
    		return forward(me,in_port,1);
    	}
    }
    if(dpid==3){
    	if(dl_type == 0x0800){
    		//update_checksum(pkt);
    		uint8_t len=0x05;
    		uint64_t hash=0x1234567890abcdef;
    		uint16_t selectors=0;
    		uint8_t lifetime=0;
    		uint64_t name=0;
    		header_adding(pkt,&len, &hash, &selectors, &lifetime, &name);
    		return forward(me,in_port,1);
    	}
    }
	return route();
}
