#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	//dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);

	//uint64_t src = value_to_48(read_packet(pkt, "dl_src"));
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

	if(dl_type == 0x0800) {
		pull_header(pkt);	// goto IPv4
		uint32_t nw_dst = value_to_32(read_packet(pkt, "nw_dst"));
		push_header(pkt);
	}
	if(in_port == 1) return forward(me, in_port, 2);
	else return forward(me, in_port, 1);
}

