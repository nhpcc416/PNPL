#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

void header_adding(struct packet *pkt,uint8_t vihl, uint8_t tos, uint16_t len, uint16_t id, uint16_t flagoff, uint8_t ttl, uint8_t nw_proto,
		uint16_t sum, uint32_t nw_src, uint32_t nw_dst){
	add_field(pkt, 8*14, 8*1, value_from_8(vihl));
	add_field(pkt, 8*15, 8*1, value_from_8(tos));
	add_field(pkt, 8*16, 8*2, value_from_16(len));
	add_field(pkt, 8*18, 8*2, value_from_16(id));
	add_field(pkt, 8*20, 8*2, value_from_16(flagoff));
	add_field(pkt, 8*22, 8*1, value_from_8(ttl));
	add_field(pkt, 8*23, 8*1, value_from_8(nw_proto));
	add_field(pkt, 8*24, 8*2, value_from_16(sum));
	add_field(pkt, 8*26, 8*4, value_from_32(nw_src));
	add_field(pkt, 8*30, 8*4, value_from_32(nw_dst));
	mod_packet(pkt, "dl_type", value_from_16(0x0800));
	return;
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

    if(dpid==1){
	    if(dl_type == 0x6666){
	    	pull_header(pkt);	// goto icn
	    	uint64_t value_hash = value_to_64(read_packet(pkt, "hash"));
	    	push_header(pkt);
	    	if(value_hash==0x1234567890abcdee){
	    		if(lookup_CS(pkt))
	    	        return forward(me,in_port,3);
	    		else if(lookup_PIT(pkt)){
                     out_put(in_port);
	    		}
	    		else if(lookup_FIB(pkt)){
                    add_face(pkt);
	    		}
	    		else return route();
	    	}
	    	else if(value_hash==0x1234567890abcdef){
	    		if(lookup_CS(pkt))
	    	        return forward(me,in_port,4);
	    		else if(lookup_PIT(pkt)){
                     out_put(in_port);
	    		}
	    		else if(lookup_FIB(pkt)){
                    add_face(pkt);
	    		}
	    		else return route();
	    	}
	    }
    }
    if(dpid==2){
    	if(dl_type == 0x6666){
    		header_adding(pkt,0x45, 0x11, 0x1111, 0x1111, 0x1111, 0x11, 0x11, 0x1111, 0x0b000001, 0x0b000002);
    		return forward(me,in_port,1);
    	}
    }
    if(dpid==3){
    	if(dl_type == 0x6666){
    		header_adding(pkt,0x45, 0x11, 0x1111, 0x1111, 0x1111, 0x11, 0x11, 0x1111, 0x0b000001, 0x0b000003);
    		return forward(me,in_port,1);
    	}
    }

	return route();
}

