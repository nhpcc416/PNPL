"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'learn.views.home', name='home'),

    url(r'^add_spec/$', 'learn.views.add_spec', name='add_spec'),
    url(r'^read_spec/$', 'learn.views.read_spec', name='read_spec'),
    url(r'^save_spec/$', 'learn.views.save_spec', name='save_spec'),
    url(r'^show_spec/$', 'learn.views.show_spec', name='show_spec'),
    url(r'^pic_spec/$', 'learn.views.pic_spec', name='pic_spec'),

    url(r'^add_func/$', 'learn.views.add_func', name='add_func'),
    url(r'^read_func/$', 'learn.views.read_func', name='read_func'),
    url(r'^save_func/$', 'learn.views.save_func', name='save_func'),
    url(r'^show_func/$', 'learn.views.show_func', name='show_func'),

    url(r'^$','learn.views.popindex'),
    url(r'^startpop$','learn.views.startpop'),
    url(r'^begin_pop$','learn.views.begin_pop'),
    url(r'^stoppop$','learn.views.stoppop'),

    url(r'^show_compile/$', 'learn.views.show_compile', name='show_compile'),
    url(r'^copy_compile/$', 'learn.views.copy_compile', name='copy_compile'),

    url(r'^admin/', include(admin.site.urls)),
]
