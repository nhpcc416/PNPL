  var real_list_func_open = new Array();
  //var real_list = new Array();
  $(document).ready(function(){
    $("#addfunc").click(function(){
      //var text = editor_func.getValue();
    	//alert(text);
    	//editor_func.getDoc().setValue('var msg = "Hi";');
    	var NameOfFunc = $("#NameOfFunc").val();
    	var temp = NameOfFunc.indexOf(".c");
    	if(temp == NameOfFunc.length-2){
    		$.get("/add_func/",{'NameOfFunc':NameOfFunc}, function(ret){
    			if(ret == "empty"){
    			  //$("#ContentOfFunc").val("");
    			  editor_func.getDoc().setValue('');
    			  $("#FuncNameToRun").val(NameOfFunc);
    			  alert("Create Success!" + '\n' + "You can write and save the f function now!");
    			}
    			else{
    			  //$("#ContentOfFunc").val(ret);
    			  editor_func.getDoc().setValue(ret);
    			  $("#FuncNameToRun").val(NameOfFunc);
    			  alert("File already exists!" + '\n' + "We read this file for you!");
    			}
    		})
    	}
      else{
    		alert("False!" + '\n' + "Please set the Suffix as .c!");
    	}
    });
    $("#readfunc").click(function(){
      var NameOfFunc = $("#NameOfFunc").val();
    	$.get("/read_func/",{'NameOfFunc':NameOfFunc}, function(ret){
    		if(ret == "empty"){
    			alert("File not exists!" + '\n' + "You must create this file first!" + '\n' + "Try the add button!");
    		}
    		else{
    			//$("#ContentOfFunc").val(ret);
    			editor_func.getDoc().setValue(ret);
    			$("#FuncNameToRun").val(NameOfFunc);
    		}
    	})
    });
    $("#savefunc").click(function(){
      var NameOfFunc = $("#NameOfFunc").val();
    	//var ContentOfFunc = $("#ContentOfFunc").val();
    	var ContentOfFunc = editor_func.getValue();
    	$.get("/save_func/",{'NameOfFunc':NameOfFunc, 'ContentOfFunc':ContentOfFunc}, function(ret){
    	  if(ret == "no"){
    			alert("File not exists!" + '\n' + "You must create this file first!" + '\n' + "Try the add button!");
    		}
    		else{
    			$("#FuncNameToRun").val(NameOfFunc);
    			alert("save success!");
    		}
    	})
    });
    $("#showfunc").click(function(){
      var NameOfFunc = $("#NameOfFunc").val();
    	$.get("/show_func/",{'NameOfFunc':NameOfFunc}, function(ret){
        ret = eval('(' + ret + ')');
        var func_name_list = ret.func_name_list;
        var real_list = new Array();
        //real_list = [];
        real_list_func_open = [];
        for(i = 0;i < func_name_list.length; i++){
          var temp = func_name_list[i].indexOf(".c");
    	    if(temp == func_name_list[i].length-2){
    			  j = real_list.push(func_name_list[i]);
    			}
        }
        real_list_func_open = real_list;
        var table=$("<table class=\"table table-bordered\">");
        $("#createtable_func").html(table);
        var tr_header = $("<tr></tr>");
        tr_header.appendTo(table);
        var td_header=$("<td>"+"num"+"</td>"+"<td>"+"name"+"</td>"+"<td>"+"operation"+"</td>");
        td_header.appendTo(tr_header);
        for(var i=0;i<real_list.length;i++){
          var tr=$("<tr></tr>");
          tr.appendTo(table);
          num_of_func=i+1;
          var td=$("<td>"+num_of_func+"</td>"+"<td>"+real_list[i]+"</td>"+"<td>"+"<button type=\"button\" class=\"btn btn-default btn-xs\" onclick=\"List_Open_Func(this)\">"+"<span class=\"glyphicon glyphicon-open\">"+"</span>"+"open"+"</button>"+"</td>");
          td.appendTo(tr);
        }
        $("#createtable_func").append("</table>");
    		//alert(real_list.length+" files exists.");
    	})
    });
  });
  function List_Open_Func(index){
    currentStep=$(index).parent().parent().find("td:first-child").html();
    //alert(real_list_func_open[currentStep-1]);
    $("#NameOfFunc").val(real_list_func_open[currentStep-1]);
    $("#readfunc").click();
  }