# -*- coding:utf-8 -*-

import os
import json
import subprocess
import sys
import time
import threading
import Queue
import shutil

from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render, render_to_response
from django.template import RequestContext, Template
from pywebsocketserver.server import SocketServer
from pywebsocketserver.baseio import BaseIO

# Create your views here.
primary_path='/home/ck/C/POP/POPGit2/POP/'
pic_index = 0
def home(request):
    return render(request, 'home.html')

def add_spec(request):
    NameOfSpec = request.GET['NameOfSpec']
    add_path = primary_path+'/popfile/spec/'
    open_path = add_path + NameOfSpec
    spec_name_list = os.listdir(add_path)
    if NameOfSpec in spec_name_list:
        f = open(open_path)
        spec_list = f.readlines()
        f.close()
        return HttpResponse(spec_list)
    else:
        f = open(open_path, 'w')
        f.close()
        return HttpResponse(u'empty')

def read_spec(request):
    NameOfSpec = request.GET['NameOfSpec']
    add_path = primary_path+'/popfile/spec/'
    spec_name_list = os.listdir(add_path)
    if NameOfSpec in spec_name_list:
        #spec_path = os.getcwd()
        f = open(add_path + NameOfSpec)
        spec_list = f.readlines()
        f.close()
        #list_of_spec = ['header.spec','header1.spec','header2.spec']
        return HttpResponse(spec_list)
    else:
        return HttpResponse(u'empty')

def save_spec(request):
    NameOfSpec = request.GET['NameOfSpec']
    ContentOfSpec = request.GET['ContentOfSpec']
    add_path = primary_path+'/popfile/spec/'
    spec_name_list = os.listdir(add_path)
    if NameOfSpec in spec_name_list:
        f = open(add_path + NameOfSpec, 'w')
        f.write(ContentOfSpec)
        #spec_list = f.readlines()
        f.close()
        return HttpResponse(u'yes')
    else:
        return HttpResponse(u'no')

def show_spec(request):
    add_path = primary_path+'/popfile/spec/'
    spec_name_list = os.listdir(add_path)
    return HttpResponse(json.dumps({'spec_name_list': spec_name_list}))

def pic_spec(request):
    NameOfSpec = request.GET['NameOfSpec']
    pic_path = primary_path+'/gparser/'
    global pic_index
    pic_name = 'test' + str(pic_index) + '.png'

    src_pic = primary_path+'/popfile/spec/'+NameOfSpec
    dst_pic = primary_path+'/gparser/examples/header'
    if os.path.exists(dst_pic):
        os.remove(dst_pic)
    shutil.copy(src_pic,dst_pic)
    if os.path.exists(pic_path+'test.png'):
        os.remove(pic_path+'test.png')
    os.chdir(pic_path)
    popen=subprocess.Popen(["src/gparser","examples/header","test.png"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #time.sleep(5)
    err = popen.stderr.read()
    pic_list = []
    if os.path.exists(pic_path+'test.png'):
        dst_pic_copy = primary_path+'/UI_new/static/'+pic_name
        if os.path.exists(dst_pic_copy):
            os.remove(dst_pic_copy)
        shutil.copy(pic_path+'test.png',dst_pic_copy)
        pic_list.append('yes')
        pic_list.append(pic_index)
        pic_index = pic_index + 1
        pic_index = pic_index % 2
    else:
        pic_list.append('no')
        pic_list.append(err)
    return HttpResponse(json.dumps({'pic_list': pic_list}))

def add_func(request):
    NameOfFunc = request.GET['NameOfFunc']
    add_path = primary_path+'/popfile/func/'
    open_path = add_path + NameOfFunc
    func_name_list = os.listdir(add_path)
    if NameOfFunc in func_name_list:
        f = open(open_path)
        func_list = f.readlines()
        f.close()
        return HttpResponse(func_list)
    else:
        f = open(open_path, 'w')
        f.close()
        return HttpResponse(u'empty')

def read_func(request):
    NameOfFunc = request.GET['NameOfFunc']
    add_path = primary_path+'/popfile/func/'
    func_name_list = os.listdir(add_path)
    if NameOfFunc in func_name_list:
        #spec_path = os.getcwd()
        f = open(add_path + NameOfFunc)
        func_list = f.readlines()
        f.close()
        #list_of_spec = ['header.spec','header1.spec','header2.spec']
        return HttpResponse(func_list)
    else:
        return HttpResponse(u'empty')

def save_func(request):
    NameOfFunc = request.GET['NameOfFunc']
    ContentOfFunc = request.GET['ContentOfFunc']
    add_path = primary_path+'/popfile/func/'
    func_name_list = os.listdir(add_path)
    if NameOfFunc in func_name_list:
        f = open(add_path + NameOfFunc, 'w')
        f.write(ContentOfFunc)
        #spec_list = f.readlines()
        f.close()
        return HttpResponse(u'yes')
    else:
        return HttpResponse(u'no')

def show_func(request):
    add_path = primary_path+'/popfile/func/'
    func_name_list = os.listdir(add_path)
    return HttpResponse(json.dumps({'func_name_list': func_name_list}))

def show_compile(request):
    FuncNameToRun = request.GET['FuncNameToRun']
    src_func = primary_path+'/popfile/func/'
    #so_file = 'POPUI.so'
    #a,b = os.path.splitext(FuncNameToRun)
    new_folder = primary_path+'/pop_metadata/apps/POPUI/'
    if os.path.exists(new_folder):
        shutil.rmtree(new_folder)
    os.makedirs(new_folder)

    make_content = ['TOPDIR = ../..\n',
                    'EXTRA_CFLAGS = -shared -g -O2 -Wall -I../lib\n',
                    'EXTRA_LDFLAGS = -L../lib -lapps\n',
                    'SRCS = '+'POPUI.c'+'\n',
                    'PROG = '+'../POPUI.so'+'\n',
                    '\n',
                    'include ${TOPDIR}/Makefile.comm']
    shutil.copy(src_func+FuncNameToRun,new_folder+'POPUI.c')
    f = open(new_folder + 'makefile', 'w')
    f.writelines(make_content)
    f.close()
    os.chdir(new_folder)
    popen=subprocess.Popen("make",stdout=subprocess.PIPE)
    fp = popen.stdout.read()
    return HttpResponse(fp)

def copy_compile(request):
    SpecNameToRun = request.GET['SpecNameToRun']
    FuncNameToRun = request.GET['FuncNameToRun']
    src_spec = primary_path+'/popfile/spec/'+SpecNameToRun
    spec_file = 'POPUI.spec'
    dst_spec = primary_path+'/pop_metadata/scripts/'+spec_file
    if SpecNameToRun != '':
        if os.path.exists(dst_spec):
            os.remove(dst_spec)
        shutil.copy(src_spec,dst_spec)
        return HttpResponse(u'yes')
    else:
        return HttpResponse(u'no')

g_output_log=None
g_uid=None
websocket_run=False
pipe = '/tmp/pop_ws'

global pop_process,readpipe_process

g_output_info=None
g_info_uid=None
websocket_info_run=False

def readpipe():

    global g_output_info
    g_output_info=Queue.Queue()
    while (True):
        try:
            f = open("/tmp/pop_ws", "r")
            if f!=None:
                break
            else:
                continue
        except:
            continue
    while True:
        s = f.readline()
        if s == "":
		    break
        print 'line: ', s
        g_output_info.put(s)
    print ("ReadPipe Down")

#def tcp(ParameterToRun):
def tcp():
    global g_output_log,pop_process
    global pipe
    if os.path.exists(pipe):
            os.unlink(pipe)
    os.mkfifo(pipe)

	
	#ParameterToRun = request.GET['ParameterToRun']
	#SpecNameToRun = request.GET['SpecNameToRun']
	
    os.chdir(primary_path+'/pop_metadata/')
    popen=subprocess.Popen(["main/controller","-s","scripts/POPUI.spec","-f","apps/POPUI.so"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    """
        To do : start the readpipe thread
    """
    print("====================================================")
    #run readpipe
    readpipe_thread = threading.Thread(name='readpipe', target=readpipe)
    readpipe_thread.start()
    pid=popen.pid
    pop_process=popen
    print('Popen.pid:'+str(pid))
    last_line=None
    while True:

        line=popen.stderr.readline().strip()
        if line=="\n" and line==last_line:
            pass
        else:

            print "output:%s" %(line)
            g_output_log.put(line)

        last_line=line
        if subprocess.Popen.poll(popen) is not None:
                break
    print('DONE')



class MyIO(BaseIO):
    def onData(self,uid,text):
        self.sendData(uid,"received the message：%s"%(text,))
    def onConnect(self,uid):
        global g_uid
        g_uid=uid
        while True:
            #self.sendData(uid,"testing...")
            try:
                log = g_output_log.get()
                self.sendData(uid,log)
            except:
                time.sleep(.01)

class MyIOforInfo(BaseIO):
    def onData(self,uid,text):
        self.sendData(uid,"received the message：%s"%(text,))
    def onConnect(self,uid):
        # global g_uid
        # g_uid=uid
        while True:
            #self.sendData(uid,"testing...")
            try:
                log = g_output_info.get()
                self.sendData(uid,log)
            except:
                time.sleep(.01)

def popindex(request):
    return render_to_response('home.html', {})

def websocketserver():
    myIo = MyIO()
    port=8182
    SocketServer(port,myIo).run()

def infoserver():
    infoIo=MyIOforInfo()
    port=8183
    SocketServer(port,infoIo).run()

def begin_pop(request):
    try:
        global g_output_log,g_uid,websocket_run
        g_output_log=Queue.Queue()
        g_uid=None
        ParameterToRun = request.GET['ParameterToRun1']
        #pof = threading.Thread(name='pof', target=tcp, args=(ParameterToRun,))
        pof = threading.Thread(name='pof', target=tcp)
        pof.start()
        port = 8182

    except:
        port = 8182
        print "error in pof_inid"
    myIo = MyIO()
    if not websocket_run:
        websocket_run=True
        websocket_thread= threading.Thread(name='socket', target=websocketserver)
        websocket_thread.start()
        #SocketServer(port,myIo).run()
        #websocket_run=True
        websocket_info_thread=threading.Thread(name='info_socket', target=infoserver)
        websocket_info_thread.start()
    return render_to_response('js_outcome.html', RequestContext(request,{'outcome':'FinishButDontPrint'}))


def stoppop(request):
    global pop_process
    #pop_process.terminate()
    pop_process.kill()
    if os.path.exists(pipe):
        os.unlink(pipe)
    return render_to_response('js_outcome.html', RequestContext(request,{'outcome':'FinishButDontPrint'}))
