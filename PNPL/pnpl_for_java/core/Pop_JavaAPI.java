package core;
public class Pop_JavaAPI{
	
	public Pop_JavaAPI(){
	}
    //packet operation API
	public static native int j_int_read_packet_inport(int p_packet);
	public static native int j_pEntity_read_packet_inswitch(int p_packet);
	public static native int j_pPacket_pull_header(int p_packet);
    public static native String j_string_read_header_type(int p_packet);
    public static native long j_int_read_packet(int p_packet, String p_field, int length);
    public static native boolean j_bool_test_equal(int p_packet, String p_field, int p_value, int length);
    public static native int j_pUint8t_read_payload_length(int p_packet);
	public static native int j_pUint8t_read_payload(int p_packet);
	public static native void j_void_mod_packet(int p_packet, int p_field, int p_value);
	public static native int j_pPacket_push_header(int p_packet);
	public static native void j_void_add_header(int p_packet, String p_proto);
	public static native void j_void_add_field(int p_packet, int offb, int lenb, long p_value);
	public static native void j_void_del_field(int p_packet, int offb, int lenb);

    //topo search API
    public static native int j_pEntity_get_entity(int p_entity, int port);
    public static native String j_string_get_entity_type(int p_entity);
    public static native int j_int_get_switch_dpid(int p_entity);
	public static native int j_int_get_host_sw_port(int p_entity, int p_sw_port);
    public static native int j_pEntity_get_host_adj_switch(int p_entity);
    public static native int j_pEntityadj_get_entity_adjs_num(int p_entity, int p_pnum);
    public static native int j_pEntityadj_get_entity_adjs(int p_entity);
    public static native void j_void_print_entity(int p_entity);
    public static native int j_int_get_switches_num();
	public static native int j_pEntity_get_switches();
	public static native int j_pEntity_get_switch(int p_dpid);
	public static native int j_pEntity_get_host_by_paddr(long p_addr);
    public static native int j_pEntity_get_host_by_haddr(long p_addr);

}
