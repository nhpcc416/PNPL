#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <dlfcn.h>
#include "xlog/xlog.h"
#include "xswitch/xswitch-private.h"
#include "topo/topo.h"
#include "topo/entity.h"
#include "route.h"

#include "core.h"
#include "packet_parser.h"
#include "spec_parser.h"
#include "trace.h"
#include "trace_tree.h"
#include "map.h"

#include "apps/lib/spanning_tree.c"

#include "pop_api.h"
#include "jni.h"
 JNIEnv *jnienv;
 JavaVM *jvm;
 jclass cls;
 jmethodID method;
 int isJava=0;




JNIEnv* create_vm(JavaVM ** jvm)
{
	JNIEnv *jnienv = NULL;
	JavaVMInitArgs vm_args;
	JavaVMOption options;
	options.optionString = "-Djava.class.path=/home/milktank/workspace/pofswithc1.4/pop_20151226/pop_for_java"; //Path to the java source code
	vm_args.version = JNI_VERSION_1_6; //JDK version. This indicates version 1.6
	vm_args.nOptions = 1;
	vm_args.options = &options;
	vm_args.ignoreUnrecognized = 0;

	int ret = JNI_CreateJavaVM(jvm, (void**)&jnienv, &vm_args);
	if(ret < 0)
	{
		printf("\nUnable to Launch JVM\n");
		return NULL;
	}
	return jnienv;
}

/*
 * java_api
 */

//packet operation API
jint   j_int_read_packet_inport(JNIEnv *env, jclass this , jint  p_packet )
{
	struct packet *pkt=(struct packet *)p_packet;
	jint result = read_packet_inport(pkt);
	return result;
}

jint  j_pEntity_read_packet_inswitch(JNIEnv *env, jclass this , jint  p_packet )
{
	struct packet *pkt=(struct packet *)p_packet;
	jint result = (jint)topo_get_switch(pkt->in_sw->dpid);
	return result;
}

jint j_pPacket_pull_header(JNIEnv *env, jclass this, jint p_packet)
{
	printf("in the j_pPacket_pull_header.\n");
    struct packet *pkt=(struct packet *)p_packet;
    jint result = (jint)pull_header_re(pkt);
    return result;
}

jstring j_string_read_header_type(JNIEnv *env, jclass this, jint p_packet)
{
    struct packet *pkt=(struct packet *)p_packet;
	char *header_type = read_header_type(pkt);
	//jint result = 1;
	jstring result = (*env)->NewStringUTF(env, header_type);
	//jstring result = "testjstring";
	return result;
}

jlong j_int_read_packet(JNIEnv *env, jclass this, jint p_packet, jstring p_field, jint length)
{
    struct packet *pkt=(struct packet *)p_packet;
    char *field=(*env)->GetStringUTFChars(env, p_field, false);
    //int length=0;
    //int offset=0;
    jlong result;

    printf("the char is %s\n", field);
    value_t temp = read_packet(pkt, field);
    printf("the temp is %02X\n", temp);

    //header_get_field(pkt, field, &offset, &length);
    printf("after header_get_field the length is:\n", length);
    switch(length){
    case 8:
    {
		uint8_t temp8 = value_to_8(temp);
		printf("the temp8 is %02X\n", temp8);
		result = (jint)temp8;
		return result;
    }
    case 16:
    {
		uint16_t temp16 = value_to_16(temp);
		printf("the temp16 is %02X\n", temp16);
		result = (jint)temp16;
		return result;
    }
    case 32:
    {
    	uint32_t temp32 = value_to_32(temp);
		printf("the temp32 is %02X\n", temp32);
		result = (jint)temp32;
		return result;
    }
    case 48:
    {
    	uint64_t temp48 = value_to_48(temp);
		printf("the temp48 is %02X\n", temp48);
		result = (jint)temp48;
		return result;
    }
    case 64:
    {
		uint64_t temp64 = value_to_64(temp);
		printf("the temp64 is %02X\n", temp64);
		result = (jint)temp64;
		return result;
    }
    }
    assert(0);
}

jboolean j_bool_test_equal(JNIEnv *env, jclass this, jint p_packet, jstring p_field, jint p_value, jint length)
{
	struct packet *pkt=(struct packet *)p_packet;
	char *field=(*env)->GetStringUTFChars(env, p_field, false);
	printf("the field is %s\n", field);
	//char *value=(*env)->GetStringUTFChars(env, p_value, false);
	value_t value;
	jboolean result;
	bool b_result;
	switch(length){
	case 8:
	{
		uint8_t temp8 = (uint8_t)p_value;
		printf("the temp8 is %02X\n", temp8);
		value = value_from_8(temp8);
		printf("the value is %02X\n", value);
		b_result = test_equal(pkt, field, value);
		printf("the b_result is %d\n", b_result);
		//result = (jboolean)test_equal(pkt, field, value);
		result = (jboolean)b_result;
		printf("the result is %d\n", result);
		return result;
	}
	case 16:
	{
		uint16_t temp16 = (uint16_t)p_value;
		printf("the temp16 is %02X\n", temp16);
		value = value_from_16(temp16);
		printf("the value is %02X\n", value);
		result = (jboolean)test_equal(pkt, field, value);
		return result;
	}
	case 32:
	{
		uint32_t temp32 = (uint32_t)p_value;
		printf("the temp32 is %02X\n", temp32);
		value = value_from_32(temp32);
		printf("the value is %02X\n", value);
		result = (jboolean)test_equal(pkt, field, value);
		return result;
	}
	case 48:
	{
		uint64_t temp48 = (uint64_t)p_value;
		printf("the temp48 is %02X\n", temp48);
		value = value_from_48(temp48);
		printf("the value is %02X\n", value);
		result = (jboolean)test_equal(pkt, field, value);
		return result;
	}
	case 64:
	{
		uint64_t temp64 = (uint64_t)p_value;
		printf("the temp64 is %02X\n", temp64);
		value = value_from_64(temp64);
		printf("the value is %02X\n", value);
		result = (jboolean)test_equal(pkt, field, value);
		return result;
	}
	}
	assert(0);
}

jint j_pUint8t_read_payload_length(JNIEnv *env, jclass this, jint p_packet)
{
	struct packet *pkt=(struct packet *)p_packet;
	int p_length = 0;
	jint result = (jint)read_payload(pkt, &p_length);
	return p_length;
}

jint j_pUint8t_read_payload(JNIEnv *env, jclass this, jint p_packet)
{
	struct packet *pkt=(struct packet *)p_packet;
	int p_length = 0;
	jint result = (jint)read_payload(pkt, &p_length);
	return result;
}

void j_void_mod_packet(JNIEnv *env, jclass this, jint p_packet, jint p_field, jint p_value)
{
	struct packet *pkt=(struct packet *)p_packet;
	char *field=(char *)p_field;
	value_t *value=(struct value_t *)p_value;
	mod_packet_p_value(pkt, field, value);
	return;
}

jint j_pPacket_push_header(JNIEnv *env, jclass this, jint p_packet)
{
    struct packet *pkt=(struct packet *)p_packet;
    jint result = (jint)push_header_re(pkt);
    return result;
}

void j_void_add_header(JNIEnv *env, jclass this, jint p_packet, jstring p_proto)
{
    struct packet *pkt=(struct packet *)p_packet;
    char *proto=(*env)->GetStringUTFChars(env, p_proto, false);
    add_header(pkt, proto);
    return;
}

void j_void_add_field(JNIEnv *env, jclass this, jint p_packet, jint offb, jint lenb, jlong p_value)
{
    struct packet *pkt=(struct packet *)p_packet;
    //char *proto=(*env)->GetStringUTFChars(env, p_proto, false);
    value_t value;
    switch(lenb){
	case 8:
	{
		uint8_t temp8 = (uint8_t)p_value;
		value = value_from_8(temp8);
		break;
	}
	case 16:
	{
		uint16_t temp16 = (uint16_t)p_value;
		value = value_from_16(temp16);
		break;
	}
	case 32:
	{
		uint32_t temp32 = (uint32_t)p_value;
		value = value_from_32(temp32);
		break;
	}
	case 48:
	{
		uint64_t temp48 = (uint64_t)p_value;
		printf("the temp48 is %02X\n", temp48);
		value = value_from_48(temp48);
		break;
	}
	case 64:
	{
		uint64_t temp64 = (uint64_t)p_value;
		printf("the temp64 is %02X\n", temp64);
		value = value_from_64(temp64);
		break;
	}
	}
    add_field(pkt, offb, lenb, value);
    return;
}

void j_void_del_field(JNIEnv *env, jclass this, jint p_packet, jint offb, jint lenb)
{
    struct packet *pkt=(struct packet *)p_packet;
    del_field(pkt, offb, lenb);
    return;
}


//topo search API
jint j_pEntity_get_entity(JNIEnv *env, jclass this, jint p_entity, jint port)
{
    struct entity *p=(struct entity *)p_entity;
    jint result = (jint)get_entity(p, port);
    return result;
}

jstring j_string_get_entity_type(JNIEnv *env, jclass this, jint p_entity)
{
	struct entity *p=(struct entity *)p_entity;
	enum entity_type enum_result;
	enum_result = get_entity_type(p);
	jstring result = "ENTITY_TYPE_HOST";
	if(enum_result == ENTITY_TYPE_SWITCH)
	{
		result = ENTITY_TYPE_SWITCH;
	}
	return result;
}

jint  j_int_get_switch_dpid(JNIEnv *env, jclass this , jint  p_entity )
{
	struct entity *p=(struct entity *)p_entity;
	jint result = get_switch_dpid(p);
	return result;
}

jint j_int_get_host_sw_port(JNIEnv *env, jclass this, jint p_entity, jint p_sw_port)
{
	struct entity *p=(struct entity *)p_entity;
	//int p_sw_port = 0;
	const struct entity_adj *adjs = entity_get_adjs(p, &p_sw_port);
	p_sw_port = adjs[0].adj_in_port;
	printf("the p_sw_port is:%d\n", p_sw_port);
	return p_sw_port;
}

jint j_pEntity_get_host_adj_switch(JNIEnv *env, jclass this, jint p_entity)
{
    struct entity *p=(struct entity *)p_entity;
    //int *sw_port=(int *)p_sw_port;
    int p_sw_port = 0;
    jint result = (jint)get_host_adj_switch(p, &p_sw_port);
    return result;
}





jint j_pEntityadj_get_entity_adjs_num(JNIEnv *env, jclass this, jint p_entity, jint p_pnum)
{
    struct entity *p=(struct entity *)p_entity;
    //int *pnum=(int *)p_pnum;
    const struct entity_adj *adjs = entity_get_adjs(p, &p_pnum);
    //p_pnum = p->num_adjs;
    return p_pnum;
}

jint j_pEntityadj_get_entity_adjs(JNIEnv *env, jclass this, jint p_entity)
{
	int p_pnum = 0;
    struct entity *p=(struct entity *)p_entity;
    //int *pnum=(int *)p_pnum;
    jint result = (jint)get_entity_adjs(p, &p_pnum);
    return result;
}

void j_void_print_entity(JNIEnv *env, jclass this, jint p_entity)
{
    struct entity *p=(struct entity *)p_entity;
    print_entity(p);
    return;
}

jint j_int_get_switches_num(JNIEnv *env, jclass this)
{
	int pnum = 0;
	struct entity **switches = topo_get_switches(&pnum);
	//p_pnum = *pnum;
	printf("the num1 is:%d\n", pnum);
	return pnum;
}

jint j_pEntity_get_switches(JNIEnv *env, jclass this)
{
	//printf("between switches\n");
	int pnum = 0;
	jint result = (jint)get_switches(&pnum);
	printf("the num2 is:%d\n", pnum);
	return result;
}

jint j_pEntity_get_switch(JNIEnv *env, jclass this, jint p_dpid)
{
	uint32_t temp32 = (uint32_t)p_dpid;
    jint result = (jint)get_switch(temp32);
    return result;
}

jint j_pEntity_get_host_by_paddr(JNIEnv *env, jclass this, jlong p_addr)
{
    uint32_t addr=(uint32_t)p_addr;
    printf("the addr is:%02X\n", addr);
    jint result = (jint)get_host_by_paddr(addr);
    return result;
}

jint j_pEntity_get_host_by_haddr(JNIEnv *env, jclass this, jlong p_addr)
{
    uint64_t temp48 = (uint64_t)p_addr;
    value_t value = value_from_48(temp48);
    haddr_t addr = value_to_haddr(value);
	jint result = (jint)get_host_by_haddr(addr);
	return result;
}

JNINativeMethod methods[] = {

	{"j_int_read_packet_inport","(I)I",j_int_read_packet_inport},
	{"j_pEntity_read_packet_inswitch","(I)I",j_pEntity_read_packet_inswitch},
	{"j_pPacket_pull_header","(I)I",j_pPacket_pull_header},
	{"j_string_read_header_type","(I)Ljava/lang/String;",j_string_read_header_type},
	{"j_int_read_packet","(ILjava/lang/String;I)J",j_int_read_packet},
	{"j_bool_test_equal","(ILjava/lang/String;II)Z",j_bool_test_equal},
	{"j_pUint8t_read_payload_length","(I)I",j_pUint8t_read_payload_length},
	{"j_pUint8t_read_payload","(I)I",j_pUint8t_read_payload},
	{"j_void_mod_packet","(III)V",j_void_mod_packet},
	{"j_pPacket_push_header","(I)I",j_pPacket_push_header},
	{"j_void_add_header","(ILjava/lang/String;)V",j_void_add_header},
	{"j_void_add_field","(IIIJ)V",j_void_add_field},
	{"j_void_del_field","(III)V",j_void_del_field},

	{"j_pEntity_get_entity","(II)I",j_pEntity_get_entity},
	{"j_string_get_entity_type","(I)Ljava/lang/String;",j_string_get_entity_type},
	{"j_int_get_switch_dpid", "(I)I", j_int_get_switch_dpid},
	{"j_int_get_host_sw_port","(II)I",j_int_get_host_sw_port},
	{"j_pEntity_get_host_adj_switch","(I)I",j_pEntity_get_host_adj_switch},
	{"j_pEntityadj_get_entity_adjs_num","(II)I",j_pEntityadj_get_entity_adjs_num},
	{"j_pEntityadj_get_entity_adjs","(I)I",j_pEntityadj_get_entity_adjs},
	{"j_void_print_entity","(I)V",j_void_print_entity},
	{"j_int_get_switches_num","()I",j_int_get_switches_num},
	{"j_pEntity_get_switches","()I",j_pEntity_get_switches},
	{"j_pEntity_get_switch","(I)I",j_pEntity_get_switch},
	{"j_pEntity_get_host_by_paddr","(J)I",j_pEntity_get_host_by_paddr},
	{"j_pEntity_get_host_by_haddr","(J)I",j_pEntity_get_host_by_haddr},

};

//router api
jint  j_pRoute_route(JNIEnv *env, jclass this )
{
	jint result = (jint)route();
	return result;
}

void j_void_route_free(JNIEnv *env, jclass this, jint p_route)
{
    free((struct route *)p_route);
    return;
}

void  j_void_route_add_edge(JNIEnv *env, jclass this , jint  p_route,jint p_edge)
{

	route_add_p_edge((struct route *)p_route,(edge_t *)p_edge);
	return ;
}

void j_void_route_union(JNIEnv *env, jclass this, jint p_route1, jint p_route2)
{
    route_union((struct route *)p_route1, (struct route *)p_route2);
    return;
}

jint  j_pEdge_edge(JNIEnv *env, jclass this,jint p_ent1,jint port1,jint p_ent2,jint port2 )
{
	jint result = (jint)create_edge((struct entity*)p_ent1,(int)port1,(struct entity *)p_ent2,(int)port2);
	return result;
}



jint j_pNodeinfo_get_tree(JNIEnv *env, jclass this, jint p_src, jint src_port, jint p_dst, jint p_switches, jint switches_num)
{
	struct entity *src=(struct entity *)p_src;
	struct entity *dst=(struct entity *)p_dst;
	struct entity **switches=(struct entity *)p_switches;
	jint result = (jint)get_tree(src, src_port, dst, switches, switches_num);
	return result;
}

jint j_pRoute_get_route(JNIEnv *env, jclass this, jint p_dst, jint dst_port, jint p_visited, jint p_switches, jint switches_num)
{
	struct entity *dst=(struct entity *)p_dst;
	struct nodeinfo *visited=(struct nodeinfo *)p_visited;
	struct entity **switches=(struct entity *)p_switches;
	jint result = (jint)get_route(dst, dst_port, visited, switches, switches_num);
	return result;
}



JNINativeMethod Routemethods[] = {
	{"j_pRoute_route", "()I", j_pRoute_route},
	{"j_void_route_free","(I)V",j_void_route_free},
	{"j_void_route_add_edge","(II)V",j_void_route_add_edge},
	{"j_void_route_union","(II)V",j_void_route_union},

	{"j_pEdge_edge","(IIII)I",j_pEdge_edge},
	{"j_pNodeinfo_get_tree","(IIIII)I",j_pNodeinfo_get_tree},
	{"j_pRoute_get_route","(IIIII)I",j_pRoute_get_route},



};




static void *algo_handle;

static struct header *header_spec;

static struct map *env;



/* f */
static struct route *(*f)(struct packet *pkt, struct map *env);
static void *(*init_f)(struct map *env);

/* API for f */
/* milktank move to core.h
struct packet {
	struct packet_parser *pp;
	struct xswitch *in_sw;
	int in_port;
	bool hack_get_payload;
};
*/

int read_packet_inport(struct packet *pkt)
{
	return pkt->in_port;
}

struct entity *read_packet_inswitch(struct packet *pkt)
{
	return topo_get_switch(pkt->in_sw->dpid);
}

void pull_header(struct packet *pkt)
{
	value_t sel_value;
	struct header *old_spec, *new_spec;
	int next_stack_top;
	/* XXX: hack */
	packet_parser_pull(pkt->pp, &old_spec, &sel_value, &new_spec, &next_stack_top);
	trace_R(header_get_sel(old_spec), sel_value);
	trace_G(old_spec, new_spec, next_stack_top);
	xdebug("current header: %s\n", header_get_name(new_spec));
}

//Curtis
struct packet *pull_header_re(struct packet *pkt)
{
	value_t sel_value;
	struct header *old_spec, *new_spec;
	int next_stack_top;
	/* XXX: hack */
	packet_parser_pull(pkt->pp, &old_spec, &sel_value, &new_spec, &next_stack_top);
	trace_R(header_get_sel(old_spec), sel_value);
	trace_G(old_spec, new_spec, next_stack_top);
	xdebug("current header: %s\n", header_get_name(new_spec));
	return pkt;
}

const char *read_header_type(struct packet *pkt)
{
	return packet_parser_read_type(pkt->pp);
}

value_t read_packet(struct packet *pkt, const char *field)
{
	value_t v = packet_parser_read(pkt->pp, field);
	trace_R(field, v);
	return v;
}

//Curtis
value_t *read_packet_re(struct packet *pkt, const char *field)
{
	value_t vv = packet_parser_read(pkt->pp, field);
	trace_R(field, vv);
	struct value_t *v = &vv;
	return v;
}

bool test_equal(struct packet *pkt, const char *field, value_t value)
{
	value_t v = packet_parser_read(pkt->pp, field);
	bool result = value_equal(v, value);
	trace_T(field, value, result);
	return result;
}

const uint8_t *read_payload(struct packet *pkt, int *length)
{
	pkt->hack_get_payload = true;
	return packet_parser_get_payload(pkt->pp, length);
}

void push_header(struct packet *pkt)
{
	struct header *new_spec;
	int prev_stack_top;
	packet_parser_push(pkt->pp, &new_spec, &prev_stack_top);
	trace_P(new_spec, prev_stack_top);
}

//Curtis
struct packet *push_header_re(struct packet *pkt)
{
	struct header *new_spec;
	int prev_stack_top;
	packet_parser_push(pkt->pp, &new_spec, &prev_stack_top);
	trace_P(new_spec, prev_stack_top);
	return pkt;
}

void mod_packet(struct packet *pkt, const char *field, value_t value)
{
	struct header *spec;
	packet_parser_mod(pkt->pp, field, value, &spec);
	trace_M(field, value, spec);
}

//Curtis
void mod_packet_p_value(struct packet *pkt, const char *field, value_t *value)
{
	struct header *spec;
	packet_parser_mod(pkt->pp, field, *value, &spec);
	trace_M(field, *value, spec);
}

void add_header(struct packet *pkt, const char *proto)
{
	int hlen;
	struct header *h = header_lookup(header_spec, proto);
	packet_parser_add_header(pkt->pp, h, &hlen);
	trace_A(hlen);
}

void add_field(struct packet *pkt, int offb, int lenb, value_t value)
{
	packet_parser_add_field(pkt->pp, offb, lenb, value);
	trace_AF(offb, lenb, value);
}

void del_field(struct packet *pkt, int offb, int lenb)
{
	packet_parser_del_field(pkt->pp, offb, lenb);
	trace_DF(offb, lenb);
}

struct entity **get_hosts(int *pnum)
{
	struct entity **hosts = topo_get_hosts(pnum);
	trace_RE("topo_hosts", hosts);
	return hosts;
}

struct entity **get_switches(int *pnum)
{
	struct entity **switches = topo_get_switches(pnum);
	trace_RE("topo_switches", switches);
	return switches;
}

struct entity **get_switches_int(int *pnum)
{
	//int *p_pnum = &pnum;
	struct entity **switches = topo_get_switches(pnum);
	trace_RE("topo_switches", switches);
	//pnum = *p_pnum;
	printf("the num in the get_switches_int is:%d\n",*pnum);
	return switches;
}

struct entity *get_switch(dpid_t dpid)
{
	struct entity *esw = topo_get_switch(dpid);
	trace_RE("topo_switch", esw);
	return esw;
}

struct entity *get_host_by_haddr(haddr_t addr)
{
	struct entity *eh = topo_get_host_by_haddr(addr);
	trace_RE("topo_host", eh);
	return eh;
}

struct entity *get_host_by_paddr(uint32_t addr)
{
	struct entity *eh = topo_get_host_by_paddr(addr);
	trace_RE("topo_host", eh);
	return eh;
}

//Curtis
struct entity *get_host_by_paddr_p_addr(uint32_t *addr)
{
	struct entity *eh = topo_get_host_by_paddr(*addr);
	trace_RE("topo_host", eh);
	return eh;
}

enum entity_type get_entity_type(struct entity *e)
{
	return entity_get_type(e);
}

dpid_t get_switch_dpid(struct entity *e)
{
	return entity_get_dpid(e);
}

const struct entity_adj *get_entity_adjs(struct entity *e, int *pnum)
{
	const struct entity_adj *adjs = entity_get_adjs(e, pnum);
	char buf[32];
	snprintf(buf, 32, "entity_adjs%d", *pnum);
	trace_RE(buf, adjs);
	return adjs;
}

struct entity *get_host_adj_switch(struct entity *e, int *sw_port)
{
	assert(entity_get_type(e) == ENTITY_TYPE_HOST);
	int num;
	const struct entity_adj *adjs = get_entity_adjs(e, &num);
	assert(num == 1);
	*sw_port = adjs[0].adj_in_port;
	return adjs[0].adj_entity;
}

struct entity *get_entity(struct entity *e, int port)
{
	int i, n;
	const struct entity_adj *adjs = entity_get_adjs(e, &n);

	for(i = 0; i < n; i++)
		if(adjs[i].out_port == port) {
			if(entity_get_type(adjs[i].adj_entity) == ENTITY_TYPE_HOST)
				trace_RE("topo_host", adjs[i].adj_entity);
			else
				trace_RE("topo_switch", adjs[i].adj_entity);
			return adjs[i].adj_entity;
		}
	abort();
}

void print_entity(struct entity *e)
{
	return entity_print(e);
}

uint64_t get_port_recvpkts(struct entity *e, uint16_t port_id)
{
	return xport_get_recvpkts(xport_lookup(entity_get_xswitch(e), port_id));
}

uint64_t get_port_recvbytes(struct entity *e, uint16_t port_id)
{
	return xport_get_recvbytes(xport_lookup(entity_get_xswitch(e), port_id));
}

uint64_t get_port_recent_recvpkts(struct entity *e, uint16_t port_id)
{
	return xport_get_recent_recvpkts(xport_lookup(entity_get_xswitch(e), port_id));
}

uint64_t get_port_recent_recvbytes(struct entity *e, uint16_t port_id)
{
	return xport_get_recent_recvbytes(xport_lookup(entity_get_xswitch(e), port_id));
}

/* Return false means a wrong port_id or something else.*/
bool get_port_stats(struct entity *e, uint16_t port_id,
		    uint64_t *recvpkts/*OUT*/,
		    uint64_t *recvbytes/*OUT*/,
		    uint64_t *recent_recvpkts/*OUT*/,
		    uint64_t *recent_recvbytes/*OUT*/)
{
	struct xport *xp;
	if ((xp = xport_lookup(entity_get_xswitch(e), port_id)) == NULL)
		return false;
	xport_query(xp, recvpkts, recvbytes, recent_recvpkts, recent_recvbytes);
	return true;
}

/* XXX */
/* XXX: acquire topo_lock first! */
void core_invalidate(bool (*p)(void *p_data, const char *name, const void *arg), void *p_data)
{
	int num_switches;
	struct entity **switches;
	int i;

	switches = topo_get_switches(&num_switches);
	xdebug("core_invalidate\n");
	for(i = 0; i < num_switches; i++) {
		struct xswitch *cur_sw = entity_get_xswitch(switches[i]);
		struct trace_tree *tt = cur_sw->trace_tree;

		xswitch_table_lock(cur_sw);
		trace_tree_invalidate(&tt, cur_sw, cur_sw->table0, p, p_data);
#ifdef ENABLE_WEB
		trace_tree_print_json(cur_sw->trace_tree, cur_sw->dpid);
		trace_tree_print_ft_json(cur_sw->trace_tree, cur_sw->dpid);
#endif
		xswitch_table_unlock(cur_sw);
	}
}

/* call back funtions */
void core_init(const char *algo_file, const char *spec_file)
{
	xinfo("loading algorithm(%s)...\n", algo_file);
	//milktank find the java class file
	printf ("algo_file:%s\n",algo_file);
	char *p =strstr(algo_file,".class");
	if (NULL!=p)
	{
		isJava=1;
		int len=strlen(algo_file);
		//char classfile[255]={0};
		printf("len:%d\n",strlen(algo_file));
		printf ("offset:%d\n",p-algo_file);
		if ((p-algo_file+6)==len){
			 filename=(char *) malloc (sizeof(char)*(p-algo_file+1));
					//memset(classfile,(p-algo_file+1),0);
					//strncpy(classfile,"../",3);
			strncpy(filename,algo_file,p-algo_file);
			printf ("new_class:%s\n",filename);
		}
		init_f = NULL;
	}
	else
	{
	algo_handle = dlopen(algo_file, RTLD_NOW);
	if(!algo_handle)
		xerror("error: %s\n", dlerror());
	assert(algo_handle);
	f = dlsym(algo_handle, "f");
	assert(f);
	init_f = dlsym(algo_handle, "init_f");
	assert(init_f);
	}
	xinfo("loading header spec(%s)...\n", spec_file);
	header_spec = spec_parser_file(spec_file);
	assert(header_spec);
	xinfo("init env...\n");
	env = map(mapf_eq_str, mapf_hash_str, mapf_dup_str, mapf_free_str);
	if(init_f)
		init_f(env);
}

void core_switch_up(struct xswitch *sw)
{
	/* init trace tree */
	sw->trace_tree = trace_tree();
}

void core_switch_down(struct xswitch *sw)
{
	trace_tree_free(sw->trace_tree);
}

static void mod_in_port(struct trace *trace, int in_port)
{
	int i = trace->num_events - 1;
	assert(i >= 0);
	assert(trace->events[i].type == EV_R);
	assert(strcmp(trace->events[i].u.r.name, "in_port") == 0);
	trace->events[i].u.r.value = value_from_8(in_port);
}


struct namearg
{
	const char *name;
	const void *arg;
};

static bool cmpna_p(void *parg, const char *name, const void *arg)
{
	struct namearg *na = parg;
	if(strcmp(na->name, name) == 0 && (na->arg == arg || NULL == arg))
		return true;
	return false;
}

int f_func_count=0;
void core_packet_in(struct xswitch *sw, int in_port, uint8_t *packet, int packet_len)
{
	int i;
	struct route *r;
	struct packet pkt;
	edge_t *edges;
	int num_edges;
	struct trace *trace;
	struct action *ac_edge, *ac_core;
	bool eq_edge_core;

	/* init */
	trace_clear();

	/* run */
	pkt.pp = packet_parser(header_spec, packet, packet_len);
	pkt.in_sw = sw;
	pkt.in_port = in_port;
	pkt.hack_get_payload = false;
	trace_G(NULL, header_spec, 0);

	topo_rdlock();
	if (isJava)
	{
		printf("\ncount:%d\n",f_func_count);
		f_func_count=f_func_count+1;
				if (f_func_count==1)
					{
					//jvm=NULL;
					jnienv = create_vm(&jvm);
					if (jnienv == NULL)
						return ;
					if(!(cls = (*jnienv)->FindClass(jnienv, "core/Pop_JavaAPI")))
						{
							printf("can't find Pop_JavaAPI.class:%s\n","Pop_JavaAPI");
							return ;
						}
						int length_of_methods;
						length_of_methods = sizeof(methods)/sizeof(methods[0]);
						printf("length_of_methods is: %d\n", length_of_methods);
						(*jnienv)->RegisterNatives(jnienv, cls, methods, length_of_methods);

						if(!(cls = (*jnienv)->FindClass(jnienv, "core/Pop_JavaRoute")))
						{
							printf("can't find Pop_RouteAPI.class:%s\n","Pop_RouteAPI");
							return ;
						}
						int length_of_Routemethods;
						length_of_Routemethods = sizeof(Routemethods)/sizeof(Routemethods[0]);
						printf("length_of_Routemethods is: %d\n", length_of_Routemethods);
						(*jnienv)->RegisterNatives(jnienv, cls, Routemethods, length_of_Routemethods);

						//Find the user class and the f function
						if(!(cls = (*jnienv)->FindClass(jnienv, filename)))
						{
							printf("can't find Main.class:%s\n",filename);
							return ;
						}
						else
						{
							printf("find Main.class successfully\n");
						}
						method = (*jnienv)->GetStaticMethodID(jnienv, cls, "f", "(II)I");
						//method = (*jnienv)->GetStaticMethodID(jnienv, cls, "TestPassPotinter", "(II)I");

						if (!method)
						{
							printf("method register erro");
							return ;
						}
						//jint pointer3=(*jnienv)->CallStaticIntMethod(jnienv, cls, method, 1,2);

						printf ("env:%p\n",jnienv);
						printf ("class:%p\n",cls);
						printf ("method:%p\n",method);

						printf("init jvm successfully\n");
					}




		jint p_packet=(jint)(&pkt);
		jint p_env=(jint)(env);
		printf ("\nbegin to do f function\n");

//		printf ("global_env:%p\n",jnienv);
//		printf ("global_class:%p\n",cls);
//		printf ("global_method:%p\n",method);

		printf ("p_packet:%d\n",p_packet);
		printf ("p_env:%d\n",p_env);

		//printf ("method in the outside is:%p\n",method);

		jint p_router=(*jnienv)->CallStaticIntMethod(jnienv, cls, method, p_packet, p_env);
		if (env->ExceptionOccurred()){
			printf("ExceptionOccurred\n");
		}
		//jint p_router=(*jnienv)->CallStaticIntMethod(jnienv, cls, method, 12,13);


		struct entity *test_me = read_packet_inswitch(&pkt);
		printf ("test_me in C:%d\n",test_me);
		printf ("return p_router:%d\n",p_router);


		r=(struct route *)p_router;
		printf ("return r:%d\n",r);
		//int n = (*jvm)->DestroyJavaVM(jvm);
	}
	else {
		jint p_packet=(jint)(&pkt);
		jint p_env=(jint)(env);
		printf ("p_packet:%d\n",p_packet);
		printf ("p_env:%d\n",p_env);
		r = f(&pkt, env);
	}
	topo_unlock();

	//printf ("\n**just empty route**\n");
	//r=route();

	trace_R("in_port", value_from_8(0));

	trace = trace_get();

	/* learn */
	ac_edge = action();
	ac_core = action();
	eq_edge_core = trace_generate_action(trace, ac_core, ac_edge);

	edges = route_get_edges(r, &num_edges);
	//deal with edges
	printf("num_edges:%d\n",num_edges);
	int k;
	for ( k=0;k<num_edges;k++)
	{
		struct entity *first_ent = edges[k].ent1;
		printf("handle edge (0x%x, %d, 0x%x, %d):\n", entity_get_dpid(edges[k].ent1),
			       edges[k].port1,
			       entity_get_dpid(edges[k].ent2),
			       edges[k].port2);
	}



	struct {
		struct entity *ent;
		struct xswitch *sw;
		struct action *ac;
		int in;
		enum { MODE_EDGE, MODE_CORE } mode;
	} sw_actions[num_edges];
	int n_actions = 0;
	for(i = 0; i < num_edges; i++) {
		struct entity *cur_ent = edges[i].ent1;
		int out_port = edges[i].port1;
		xdebug("handle edge (0x%x, %d, 0x%x, %d):\n",
		       entity_get_dpid(edges[i].ent1),
		       edges[i].port1,
		       entity_get_dpid(edges[i].ent2),
		       edges[i].port2);
		if(cur_ent) {
			struct xswitch *cur_sw = entity_get_xswitch(cur_ent);
			int j, entry;
			bool flag = true;
			for(j = 0; j < n_actions; j++)
				if(cur_ent == sw_actions[j].ent) {
					flag = false;
					entry = j;
					break;
				}
			if(flag) {
				entry = n_actions;
				n_actions++;
				sw_actions[entry].ent = cur_ent;
				sw_actions[entry].sw = cur_sw;
				sw_actions[entry].ac = action();
				if(edges[i].ent2 == NULL) {
					struct msgbuf *mb;
					struct action *a0 = action();
					int plen;
					const uint8_t *p = packet_parser_get_raw(pkt.pp, &plen);
					action_add(a0, AC_OUTPUT, out_port);
					mb = msg_packet_out(0, p, plen, a0);
					xswitch_send(cur_sw, mb);
					action_free(a0);
					sw_actions[entry].ac = action_copy(ac_edge);
					sw_actions[entry].mode = MODE_EDGE;
				} else {
					sw_actions[entry].ac = action_copy(ac_core);
					sw_actions[entry].mode = MODE_CORE;
				}
			}

			/* XXX */
			assert((eq_edge_core) ||
			       (sw_actions[entry].mode == MODE_EDGE && edges[i].ent2 == NULL) ||
			       (sw_actions[entry].mode == MODE_CORE && edges[i].ent2 != NULL));

			action_add(sw_actions[entry].ac, AC_OUTPUT, out_port);
			for(j = 0; j < num_edges; j++) {
				if(edges[j].ent2 == cur_ent)
					break;
			}
			assert(j < num_edges);
			sw_actions[entry].in = edges[j].port2;
		}
	}
	printf ("\n**n_action**\n");
	for(i = 0; i < n_actions; i++) {
		printf("i=%d\n", i);
		struct xswitch *cur_sw = sw_actions[i].sw;
		struct entity *cur_ent = sw_actions[i].ent;
		struct action *cur_ac = sw_actions[i].ac;
		mod_in_port(trace, sw_actions[i].in);

		xswitch_table_lock(cur_sw);
		if(trace_tree_augment(&(cur_sw->trace_tree), trace, cur_ac)) {
			xdebug("--- flow table for 0x%x ---\n", entity_get_dpid(cur_ent));
			trace_tree_print(cur_sw->trace_tree);
#ifdef ENABLE_WEB
			trace_tree_print_json(cur_sw->trace_tree, cur_sw->dpid);
#endif
			xdebug("\n");
			trace_tree_emit_rule(cur_sw, cur_sw->trace_tree);
#ifdef ENABLE_WEB
			trace_tree_print_ft_json(cur_sw->trace_tree, cur_sw->dpid);
#endif
		}
		xswitch_table_unlock(cur_sw);

		action_free(cur_ac);
	}

	action_free(ac_edge);
	action_free(ac_core);
	printf ("\n**finish emit flow**\n");
	if((!pkt.hack_get_payload) && num_edges == 0) {
		printf ("\n**drop begin**\n");
		struct action *a = action();
		mod_in_port(trace, in_port);
		action_add(a, AC_DROP, 0);

		xswitch_table_lock(sw);
		if(trace_tree_augment(&(sw->trace_tree), trace, a)) {
			printf ("\n**drop end1**\n");
			xdebug("--- flow table for cur sw ---\n");
			trace_tree_print(sw->trace_tree);
			printf ("\n**drop end2**\n");
#ifdef ENABLE_WEB
			trace_tree_print_json(sw->trace_tree, sw->dpid);
#endif
			xdebug("\n");
			trace_tree_emit_rule(sw, sw->trace_tree);
			printf ("\n**drop end3**\n");
#ifdef ENABLE_WEB
			trace_tree_print_ft_json(sw->trace_tree, sw->dpid);
#endif
		}
		xswitch_table_unlock(sw);

		action_free(a);

	}
	printf ("\n**begin free route**\n");
	route_free(r);

	/* invalidate */
	for(i = 0; i < trace->num_inv_events; i++) {
		struct namearg na;
		na.name = trace->inv_events[i].name;
		na.arg = trace->inv_events[i].arg;
		xdebug("invalidate \"%s\":\n", na.name);
		topo_rdlock();
		core_invalidate(cmpna_p, &na);
		topo_unlock();
	}

	packet_parser_free(pkt.pp);
	printf ("\n**finish one packet in**\n");
}
