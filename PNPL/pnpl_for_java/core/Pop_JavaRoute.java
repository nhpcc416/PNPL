package core;
public class Pop_JavaRoute{
	
	public Pop_JavaRoute(){
	}
	
	public  static native int j_pRoute_route();
	public static native void j_void_route_free(int p_route);
	public  static native void j_void_route_add_edge(int p_route, int p_edge);
	public static native void j_void_route_union(int p_route1, int p_route2);

	public  static native int j_pEdge_edge(int p_ent1,int port1,int p_ent2,int port2);
	public static native int j_pNodeinfo_get_tree(int p_src, int src_port, int p_dst, int p_switches, int switches_num);
	public static native int j_pRoute_get_route(int p_dst, int dst_port, int p_visited, int p_switches, int switches_num);

}
