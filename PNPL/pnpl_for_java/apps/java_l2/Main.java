package apps.java_l2;
import core.*;
public class Main 
{
	/*
	static boolean is_multicast_ip(int ip)
	{
		if((ip >> 24) >= 224)
			return true;
		return false;
	}
	*/
    public static int f(int p_packet,int p_env)
    {
		System.out.println("===============In java user function===============");
		System.out.println("p_packet in java:"+p_packet);
		System.out.println("p_env in java:"+p_env);
		
		Pop_JavaAPI api=new Pop_JavaAPI();
		Pop_JavaRoute route=new Pop_JavaRoute();
		int pEntity=api.j_pEntity_read_packet_inswitch(p_packet);
		System.out.println("entity in java:"+pEntity);
		
		int edge=0,edge2=0;
		int r=route.j_pRoute_route();
		int rr=route.j_pRoute_route();
		long hsrc_ip=0, hdst_ip=0;
		int hsrc, hdst;
		int src, dst;
		int src_port=0, dst_port=0;
		int switches_num=0;
		int switches;
		int visited;
		
		//int []collect_switch = new int[]{0,0};		
		//a[0]: switches, a[1]:switches_num
		
		//return void route;
		//System.out.println("p_route in java:"+r);
		//System.out.println("===============In java user function===============");
		//return r;
		
		
		System.out.println("before switches");
		switches_num = api.j_int_get_switches_num();
		switches = api.j_pEntity_get_switches();
		System.out.println("in the f, the switches is:"+switches);
		System.out.println("in the f, the switches_num is:"+switches_num);
		
		int enter_port=api.j_int_read_packet_inport(p_packet);
		System.out.println("inport :"+enter_port);
		
		p_packet = api.j_pPacket_pull_header(p_packet);
		System.out.println("after pull and before some api.");
		
		String current_protocol = api.j_string_read_header_type(p_packet);
		System.out.println("current_protocol:"+current_protocol);
		
		
		
		
		if(current_protocol.compareTo("ipv4") == 0)
		{
			hsrc_ip = api.j_int_read_packet(p_packet, "nw_src", 32);
			System.out.format("hsrc_ip in java: %X %n", hsrc_ip);
			System.out.println("between 2 read_packet.");
			hdst_ip = api.j_int_read_packet(p_packet, "nw_dst", 32);
			System.out.format("hdst_ip in java: %X %n", hdst_ip);
			
			if(api.j_bool_test_equal(p_packet, "nw_proto", 0x02, 8))
			{
				System.out.println("error: igmp packet.");
			}
			else
			{
				if(hdst_ip == 0xFFFFFFFF)
				{
					System.out.println("error: Broadcast Address.");
				}
				else
				{
					System.out.println("ipv4 unicast packet.");
					hsrc = api.j_pEntity_get_host_by_paddr(hsrc_ip);
					System.out.println("hsrc in java:"+hsrc);
					System.out.println("between 2 j_pEntity_get_host_by_paddr.");
					hdst = api.j_pEntity_get_host_by_paddr(hdst_ip);
					System.out.println("hdst in java:"+hdst);
					
					if(hsrc == 0 || hdst == 0)
					{
						System.out.println("ipv4_unicast: bad address.");
					}
					else
					{
						src_port = api.j_int_get_host_sw_port(hsrc, src_port);
						System.out.println("src_port in java:"+src_port);
						dst_port = api.j_int_get_host_sw_port(hdst, dst_port);
						System.out.println("dst_port in java:"+dst_port);
						
						src = api.j_pEntity_get_host_adj_switch(hsrc);
						System.out.println("src in java:"+src);
						dst = api.j_pEntity_get_host_adj_switch(hdst);
						System.out.println("dst in java:"+dst);
						
						visited = route.j_pNodeinfo_get_tree(src, src_port, dst, switches, switches_num);
						System.out.println("visited in java:"+visited);
						rr = route.j_pRoute_get_route(dst, dst_port, visited, switches, switches_num);
						System.out.println("rr in java:"+rr);
					}
				}
				
			}
			p_packet = api.j_pPacket_push_header(p_packet);
		}
		else
		{
			System.out.println("unknown protocol:" + current_protocol);
		}
		System.out.println("===============In java user function===============");
		
		return rr;
		
    }



    public static int TestPassPotinter(int a,int b)
    {
	System.out.println("succeed");
	System.out.println("in testPassPointer a+b:");
	System.out.println(a+b);
	return 1;
    }
}
