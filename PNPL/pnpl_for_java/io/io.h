#ifndef _IO_IO_H
#define _IO_IO_H

/* This file belongs to POP I/O module */

//Curtis modify NR_WORKERS: 2 -> 1
#define NR_WORKERS 1

int init_io_module(void);
void fini_io_module(void);

#endif
