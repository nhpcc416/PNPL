
header ipv4;
header tcp;
header rtmp;

header ipv4 {
	fields {
		__ver : 4;
		ihl : 4;
		__tos : 8;
		__len : 16;
		__id : 16;
		__flag : 3;
		__off : 13;
		ttl : 8;
		nw_proto : 8;
		sum : 16;
		nw_src : 32;
		nw_dst : 32;
		opt : *;
	}
	length : ihl << 2;
	checksum : sum;
	next select (nw_proto) {
		case 0x06 : tcp;
	}
}

header tcp {
	fields {
		tp_src : 16;
		tp_dst : 16;
		__seq : 32;
		__ack : 32;
        off: 4;
        flags1: 4;
		__flags2: 8;
		__win : 16;
		sum : 16;
		__urp : 16;
		opt : *;
	}
	length : off << 2;
    next select(flags1){
       case 0x0: rtmp;
    }
}

header rtmp {
	 fields {
         head_type: 8;
	     timmer: 24;
	     amfsize: 24;
	     amftype: 8;
	     id: 32;
     } 
}

start ipv4;

