"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "mtag topology example."

    def __init__( self ):
        "Create portland topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')
        s5 = self.addSwitch('s5')
        s6 = self.addSwitch('s6')
        s7 = self.addSwitch('s7')
        s8 = self.addSwitch('s8')
        s9 = self.addSwitch('s9')
        s10 = self.addSwitch('s10')
        s11 = self.addSwitch('s11')
        s12 = self.addSwitch('s12')
        s13 = self.addSwitch('s13')
        s14 = self.addSwitch('s14')
        s15 = self.addSwitch('s15')
        s16 = self.addSwitch('s16')
        s17 = self.addSwitch('s17')
        s18 = self.addSwitch('s18')
        s19 = self.addSwitch('s19')
        s20 = self.addSwitch('s20')

        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        h4 = self.addHost('h4')
        h5 = self.addHost('h5')
        h6 = self.addHost('h6')
        h7 = self.addHost('h7')
        h8 = self.addHost('h8')
        h9 = self.addHost('h9')
        h10 = self.addHost('h10')
        h11 = self.addHost('h11')
        h12 = self.addHost('h12')
        h13 = self.addHost('h13')
        h14 = self.addHost('h14')
        h15 = self.addHost('h15')
        h16 = self.addHost('h16')
        h17 = self.addHost('h17')
        h18 = self.addHost('h18')
        h19 = self.addHost('h19')
        h20 = self.addHost('h20')
        h21 = self.addHost('h21')
        h22 = self.addHost('h22')
        h23 = self.addHost('h23')
        h24 = self.addHost('h24')
        h25 = self.addHost('h25')
        h26 = self.addHost('h26')
        h27 = self.addHost('h27')
        h28 = self.addHost('h28')
        h29 = self.addHost('h29')
        h30 = self.addHost('h30')
        h31 = self.addHost('h31')
        h32 = self.addHost('h32')

        # Add links
        self.addLink(h1, s1, 1, 1)
        self.addLink(h2, s1, 1, 2)
        self.addLink(h3, s2, 1, 1)
        self.addLink(h4, s2, 1, 2)
        self.addLink(h5, s5, 1, 1)
        self.addLink(h6, s5, 1, 2)
        self.addLink(h7, s6, 1, 1)
        self.addLink(h8, s6, 1, 2)
        self.addLink(h9, s9, 1, 1)
        self.addLink(h10, s9, 1, 2)
        self.addLink(h11, s10, 1, 1)
        self.addLink(h12, s10, 1, 2)
        self.addLink(h13, s13, 1, 1)
        self.addLink(h14, s13, 1, 2)
        self.addLink(h15, s14, 1, 1)
        self.addLink(h16, s14, 1, 2)

        self.addLink(s1, s3, 3, 1)
        self.addLink(s2, s3, 3, 2)
        self.addLink(s1, s4, 4, 1)
        self.addLink(s2, s4, 4, 2)
        
        self.addLink(s5, s7, 3, 1)
        self.addLink(s6, s7, 3, 2)
        self.addLink(s5, s8, 4, 1)
        self.addLink(s6, s8, 4, 2)
        
        self.addLink(s9, s11, 3, 1)
        self.addLink(s10, s11, 3, 2)
        self.addLink(s9, s12, 4, 1)
        self.addLink(s10, s12, 4, 2)
        
        self.addLink(s13, s15, 3, 1)
        self.addLink(s14, s15, 3, 2)
        self.addLink(s13, s16, 4, 1)
        self.addLink(s14, s16, 4, 2)
        
        self.addLink(s3, s17, 3, 1)
        self.addLink(s7, s17, 3, 2)
        self.addLink(s11, s17, 3, 3)
        self.addLink(s15, s17, 3, 4)
        
        self.addLink(s3, s18, 4, 1)
        self.addLink(s7, s18, 4, 2)
        self.addLink(s11, s18, 4, 3)
        self.addLink(s15, s18, 4, 4)
        
        self.addLink(s4, s19, 3, 1)
        self.addLink(s8, s19, 3, 2)
        self.addLink(s12, s19, 3, 3)
        self.addLink(s16, s19, 3, 4)
        
        self.addLink(s4, s20, 4, 1)
        self.addLink(s8, s20, 4, 2)
        self.addLink(s12, s20, 4, 3)
        self.addLink(s16, s20, 4, 4)

topos = { 'mytopo': ( lambda: MyTopo() ) }
