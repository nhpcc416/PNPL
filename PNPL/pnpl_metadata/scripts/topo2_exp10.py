"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add 100 switches
        swlist = []
        for i in range(1,102):
        	s=self.addSwitch('s'+str(i))
        	swlist.append(s)

        # Add links
        for i in range(1, 101):
        	self.addLink(swlist[0], swlist[i], i, 1)

topos = { 'mytopo': ( lambda: MyTopo() ) }
