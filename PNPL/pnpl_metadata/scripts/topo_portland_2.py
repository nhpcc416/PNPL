"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "mtag topology example."

    def __init__( self ):
        "Create portland topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s=[0]
        for i in range(1, 21):
        	s.append(self.addSwitch('s'+str(i)))
        
        h=[0]
        for i in range(1, 33):
        	h.append(self.addHost('h'+str(i)))
        	
        # Add links
        # Add Edge to Host
        for i in range(1, 9):
        	for j in range(1, 5):
        		self.addLink(h[(i-1)*4+j], s[i], 1, j)
        
        # Add Aggregate to Edge
        for i in range(9, 17):
        	if i%2 == 1:
        		self.addLink(s[i-8], s[i], 5, 1)
        		self.addLink(s[i-7], s[i], 5, 2)
        	else:
        		self.addLink(s[i-9], s[i], 6, 1)
        		self.addLink(s[i-8], s[i], 6, 2)
        
        # Add Core to Aggregate
        self.addLink(s[9], s[17], 3, 1)
        self.addLink(s[11], s[17], 3, 2)
        self.addLink(s[13], s[17], 3, 3)
        self.addLink(s[15], s[17], 3, 4)
        
        self.addLink(s[9], s[18], 4, 1)
        self.addLink(s[11], s[18], 4, 2)
        self.addLink(s[13], s[18], 4, 3)
        self.addLink(s[15], s[18], 4, 4)
        
        self.addLink(s[10], s[19], 3, 1)
        self.addLink(s[12], s[19], 3, 2)
        self.addLink(s[14], s[19], 3, 3)
        self.addLink(s[16], s[19], 3, 4)
        
        self.addLink(s[10], s[20], 4, 1)
        self.addLink(s[12], s[20], 4, 2)
        self.addLink(s[14], s[20], 4, 3)
        self.addLink(s[16], s[20], 4, 4)
        	

topos = { 'mytopo': ( lambda: MyTopo() ) }
