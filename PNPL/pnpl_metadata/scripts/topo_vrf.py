"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s6  = self.addSwitch('s6')
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        #s4 = self.addSwitch('s4')

        num_hosts_for_each_switch=3
        s1_host=[self.addHost('h%s-s1'%i)  for i in xrange(num_hosts_for_each_switch)]
        s2_host=[self.addHost('h%s-s2'%i)  for i in xrange(num_hosts_for_each_switch)]
        s3_host=[self.addHost('h%s-s3'%i)  for i in xrange(num_hosts_for_each_switch)]

        hosts_set=[(s1_host,s1),(s2_host,s2),(s3_host,s3)]
        for  hosts_and_switch in hosts_set:
            hosts=hosts_and_switch[0]
            switch=hosts_and_switch[1]
            for host in hosts:
                self.addLink(host,switch)

        switchs=[s1,s2,s3]
        for switch in switchs:
            self.addLink(s6,switch)
	

#         # Add links
#         self.addLink(h1, s1, 1, 1)
#         self.addLink(h2, s2, 1, 1)
#         self.addLink(h3, s3, 1, 1)
#         self.addLink(h4, s4, 1, 1)
#         self.addLink(s1, s2, 2, 2)
#         self.addLink(s2, s3, 3, 3)
#         self.addLink(s3, s4, 2, 2)
#         self.addLink(s4, s1, 3, 3)
#         self.addLink(s1, s3, 4, 4)

topos = { 'mytopo': ( lambda: MyTopo() ) }
