"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "mtag topology example."

    def __init__( self ):
        "Create portland topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s=[0]
        for i in range(1, 14):
        	s.append(self.addSwitch('s'+str(i)))
        
        h=[0]
        for i in range(1, 17):
        	h.append(self.addHost('h'+str(i)))
        	
        # Add links
        # Add Edge to Host
        for i in range(1, 9):
        	for j in range(1, 3):
        		self.addLink(h[(i-1)*2+j], s[i], 1, j)
        
        # Add Aggregate to Edge
        self.addLink(s[1], s[9], 3, 1)
        self.addLink(s[2], s[9], 3, 2)
        self.addLink(s[3], s[10], 3, 1)
        self.addLink(s[4], s[10], 3, 2)
        self.addLink(s[5], s[11], 3, 1)
        self.addLink(s[6], s[11], 3, 2)
        self.addLink(s[7], s[12], 3, 1)
        self.addLink(s[8], s[12], 3, 2)
        
        # Add Core to Aggregate
        self.addLink(s[9], s[13], 3, 1)
        self.addLink(s[10], s[13], 3, 2)
        self.addLink(s[11], s[13], 3, 3)
        self.addLink(s[12], s[13], 3, 4)

topos = { 'mytopo': ( lambda: MyTopo() ) }
