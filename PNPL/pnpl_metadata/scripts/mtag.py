"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "mtag topology example."

    def __init__( self ):
        "Create mtag topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')
        s5 = self.addSwitch('s5')
        s6 = self.addSwitch('s6')
        s7 = self.addSwitch('s7')
        s8 = self.addSwitch('s8')
        s9 = self.addSwitch('s9')
        s10 = self.addSwitch('s10')
        s11 = self.addSwitch('s11')
        s12 = self.addSwitch('s12')
        s13 = self.addSwitch('s13')
        s14 = self.addSwitch('s14')

        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        h4 = self.addHost('h4')
        h5 = self.addHost('h5')
        h6 = self.addHost('h6')
        h7 = self.addHost('h7')
        h8 = self.addHost('h8')
        h9 = self.addHost('h9')
        h10 = self.addHost('h10')
        h11 = self.addHost('h11')
        h12 = self.addHost('h12')
        h13 = self.addHost('h13')
        h14 = self.addHost('h14')
        h15 = self.addHost('h15')
        h16 = self.addHost('h16')

        # Add links
        self.addLink(h1, s1, 1, 1)
        self.addLink(h2, s1, 1, 2)
        self.addLink(h3, s2, 1, 1)
        self.addLink(h4, s2, 1, 2)
        self.addLink(h5, s3, 1, 1)
        self.addLink(h6, s3, 1, 2)
        self.addLink(h7, s4, 1, 1)
        self.addLink(h8, s4, 1, 2)
        self.addLink(h9, s5, 1, 1)
        self.addLink(h10, s5, 1, 2)
        self.addLink(h11, s6, 1, 1)
        self.addLink(h12, s6, 1, 2)
        self.addLink(h13, s7, 1, 1)
        self.addLink(h14, s7, 1, 2)
        self.addLink(h15, s8, 1, 1)
        self.addLink(h16, s8, 1, 2)

        self.addLink(s1, s9, 3, 1)
        self.addLink(s2, s9, 3, 2)
        self.addLink(s3, s10, 3, 1)
        self.addLink(s4, s10, 3, 2)
        self.addLink(s5, s11, 3, 1)
        self.addLink(s6, s11, 3, 2)
        self.addLink(s7, s12, 3, 1)
        self.addLink(s8, s12, 3, 2)

        self.addLink(s9, s13, 3, 1)
        self.addLink(s9, s14, 4, 1)
        self.addLink(s10, s13, 3, 2)
        self.addLink(s10, s14, 4, 2)
        self.addLink(s11, s13, 3, 3)
        self.addLink(s11, s14, 4, 3)
        self.addLink(s12, s13, 3, 4)
        self.addLink(s12, s14, 4, 4)

topos = { 'mytopo': ( lambda: MyTopo() ) }
