#include <stdlib.h>
#include <assert.h>
#include "xlog/xlog.h"
#include "types.h"
#include "pop_api.h"
#include "route.h"


static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct route *r =NULL;
	int in_port = read_packet_inport(pkt);
	struct entity *me = read_packet_inswitch(pkt);

	uint64_t dmac;

	uint32_t d_ip;

	dmac=value_to_48(read_packet(pkt,"dl_dst"));
	pull_header(pkt);
	if(strcmp(read_header_type(pkt), "ipv4") == 0) {
			d_ip=value_to_32(read_packet(pkt,"nw_dst"));
			if (in_port==1)
            {
                push_header(pkt);
				return forward(me, in_port, 2);
            }
			else if (in_port==2)
            {
               push_header(pkt);
				return forward(me, in_port, 1);
            }
	}
	return route();
}
