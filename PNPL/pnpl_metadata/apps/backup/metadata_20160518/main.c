#include <stdlib.h>
#include <assert.h>
#include "xlog/xlog.h"
#include "types.h"
#include "pop_api.h"
#include "route.h"


static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct route *r =NULL;
	int in_port = read_packet_inport(pkt);
	struct entity *me = read_packet_inswitch(pkt);

	uint64_t dmac;
//	uint64_t metadata_dmac;
	uint32_t d_ip;

	dmac=value_to_48(read_packet(pkt,"dl_dst"));
//	metadata_dmac=value_to_48(write_metadata(pkt,"dl_src"));

	value_t metadata_dmac=write_metadata(pkt,"dl_src");

	pull_header(pkt);
	if(strcmp(read_header_type(pkt), "ipv4") == 0) {
			d_ip=value_to_32(read_packet(pkt,"nw_dst"));
			value_t s_ip=write_metadata(pkt,"nw_src");
			if(test_equal(pkt, "nw_proto", value_from_8(0x06)))
			{
					pull_header(pkt);

					read_metadata("dl_src",(&metadata_dmac));
					read_metadata("nw_src",&s_ip);
//					if(metadata_dmac>1) return forward(me, in_port, 3);

					if(strcmp(read_header_type(pkt), "tcp") == 0)
					{
						if (in_port==1)
						{
							push_header(pkt);
							push_header(pkt);
							return forward(me, in_port, 2);
						}
						else if (in_port==2)
						{
						   push_header(pkt);
						   push_header(pkt);
							return forward(me, in_port, 1);
						}
					}
			}
			else if (test_equal(pkt, "nw_proto", value_from_8(0x01)))
			{
				if (in_port==1)
										{
											push_header(pkt);

											return forward(me, in_port, 2);
										}
										else if (in_port==2)
										{
										   push_header(pkt);

											return forward(me, in_port, 1);
										}
			}
	}
	return route();
}
