#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{

	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	//uint8_t ihl = value_to_8(read_packet(pkt, "ihl"));
	uint8_t nw_proto = value_to_8(read_packet(pkt, "nw_proto"));
	printf("heben3\n");
	printf("nw_proto of ip header:%d\n",nw_proto);
	if(nw_proto == 0x06)
	{
		pull_header(pkt);
		uint8_t flags1 = value_to_8(read_packet(pkt,"flags1"));
		printf("heben2\n");
		if(flags1==0)
		{
			pull_header(pkt);
			uint8_t ramftype = value_to_8(read_packet(pkt,"amftype"));
			printf("heben1\n");
			if(ramftype==1)
			{
				printf("hello heben\n");
				int output_port=2;
				push_header(pkt);
				push_header(pkt);
				return forward(me,in_port,output_port);
			}
			push_header(pkt);
		}
		push_header(pkt);
	}
	return route();

}
