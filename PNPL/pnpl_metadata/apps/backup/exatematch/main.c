#include <stdlib.h>
#include <assert.h>
#include "xlog/xlog.h"
#include "types.h"
#include "pop_api.h"
#include "route.h"


static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}

static struct route *forward3(struct entity *esw, int in_port, int out_port1, int out_port2,int out_port3)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port1, NULL, 0));
	route_add_edge(r, edge(esw, out_port2, NULL, 0));
	route_add_edge(r, edge(esw, out_port3, NULL, 0));
	return r;
}



void init_f(struct map *env)
{
	xinfo("f init\n");
}


static struct route *flood(struct entity *esw, int in_port)
{
	int i, n;
	struct route *r = route();
	const struct entity_adj *adjs = get_entity_adjs(esw, &n);

	route_add_edge(r, edge(NULL, 0, esw, in_port));
	for(i = 0; i < n; i++) {
		int port = adjs[i].out_port;
		if(port != in_port) {
			route_add_edge(r, edge(esw, port, NULL, 0));
		}
	}
	return r;
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct route *r =NULL;
	int in_port = read_packet_inport(pkt);
	struct entity *me = read_packet_inswitch(pkt);

	uint64_t dst;

	dst=value_to_48(read_packet(pkt,"dl_dst"));

	if(dst == 0xffffffffffffull)
		return flood(me, in_port);
	if(dst ==0x1ull)
		return forward(me,in_port,1);
	if(dst ==0x2ull)
		return forward(me,in_port,2);
	if(dst ==0x3ull)
		return forward(me,in_port,3);
	if(dst ==0x4ull)
		return forward(me,in_port,4);

	return route();
}

