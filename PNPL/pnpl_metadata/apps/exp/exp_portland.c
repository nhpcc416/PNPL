#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));
	if(dl_type == 0x0800) {
		add_field(pkt, 8*14, 8*2, value_from_16(0x1234));
		add_field(pkt, 8*16, 8*1, value_from_8(0x56));
		add_field(pkt, 8*17, 8*1, value_from_8(0x78));
		add_field(pkt, 8*18, 8*2, value_from_16(0x1234));
		mod_packet(pkt, "dl_type", value_from_16(0x1234));
	}
	else if(dl_type == 0x1234) {
		//mod_packet(pkt, "dl_type", value_from_16(0x0800));
		pull_header(pkt);
		uint8_t dst_port = value_to_8(read_packet(pkt, "dst_port"));
		push_header(pkt);
		del_field(pkt, 8*18, 8*2);
		del_field(pkt, 8*17, 8*1);
		del_field(pkt, 8*16, 8*1);
		del_field(pkt, 8*14, 8*2);

		mod_packet(pkt, "dl_type", value_from_16(0x0800));
	}
	//uint64_t dl_dst = value_to_48(read_packet(pkt, "dl_src"));
	if(in_port == 1)
		return forward(me, in_port, 2);
	else return forward(me, in_port, 1);
}
