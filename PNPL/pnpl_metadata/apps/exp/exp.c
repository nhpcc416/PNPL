
//exp1: MAC NAT
struct path *f(struct packet *pkt, struct map *env) {
	struct PG_instance *pgi = apply_parse_graph(pkt);
	value_t macDst = read_packet(pgi, "mac_dst");
	if(Lookup(NATMACs, macDst) == TRUE) {				// wheather need to NAT
		mod_packet(pgi, "mac_dst", 0x554433221100);
		return CalcPath(macDst);			// Calculate path with macDst
	}
	return EmptyPath();						// Drop
}


//exp2: Block Specific ip-addresses
struct path *f(struct packet *pkt, struct map *env) {
	struct PG_instance *pgi = apply_parse_graph(pkt);
	if(pull_header(pgi, "IPv4", {}) == TRUE) {
		ipSrc = read_packet(pgi, "ip_src");
		If(Lookup(BlockIPs, ipSrc) == FALSE) {			// wheather need to Block
			return CalcPath(ipSrc);
		}
	}
	return EmptyPath();
}

//exp3: Only forward IPv4 for specific MAC addresses

//exp4: Associate IP addresses to MAC addresses

//exp5: Allow Ping from Outside to Inside

//exp6: Allow outbound DNS
struct path *f(struct packet *pkt, struct map *env) {
	declare_metadata("DstIP", 32);
	struct PG_instance *pgi = apply_parse_graph(pkt);
	if(pull_header(pgi, "IPv4", {}) == TRUE) {
		write_metadata(pgi, "ip_dst", "DstIP");
		if(pull_header(pgi, "DNS", {"DstIP"}) == TRUE) {
			value_t ipDst = read_metadata("DstIP");
			value_t dnsFlag = read_packet(pgi, "flag");
			if(Lookup(OutSideIPs, ipDst) == TRUE)		// wheather is outside
				if((dnsFlag & 0x8000) != 0)
					return CalcTwoPath(ipDst);			// generate two rules to achieve DNS request and response.
		}
	}
	return EmptyPath();
}

//exp7: Disable Outgoing Mails
struct path *f(struct packet *pkt, struct map *env) {
	declare_metadata("DstIP", 32);
	struct PG_instance *pgi = apply_parse_graph(pkt);
	if(pull_header(pgi, "IPv4", {}) == TRUE) {
		write_metadata(pgi, "ip_dst", "DstIP");
		if(pull_header(pgi, "TCP", {"DstIP"}) == TRUE) {
			value_t ipDst = read_metadata("DstIP");
			if(Lookup(OutSideIPs, ipDst) == TRUE)		// wheather is outside
				if(test_equal(pgi, "tcp_dport", 25))
					return EmptyPath();
				else if(test_equal(pgi, "tcp_dport", 465))
					return EmptyPath();
				else if(test_equal(pgi, "tcp_dport", 587))
					return EmptyPath();
			return CalcPath(ipDst);	
		}
	}
	return EmptyPath();
}

//exp8: Allow Incoming SSH from a Specific Network
struct path *f(struct packet *pkt, struct map *env) {
	declare_metadata("SrcIP", 32);
	struct PG_instance *pgi = apply_parse_graph(pkt);
	if(pull_header(pgi, "IPv4", {}) == TRUE) {
		write_metadata(pgi, "ip_src", "SrcIP");
		if(pull_header(pgi, "TCP", {"SrcIP"}) == TRUE) {
			value_t ipSrc = read_metadata("SrcIP");
			if(Lookup(OutSideIPs, ipSrc) == TRUE && Lookup(LegitimateIPs, ipSrc) == TRUE)		// wheather is outside && wheather is legal
				if(test_equal(pgi, "tcp_dport", 22))
					return CalcPath(ipSrc);	
		}
	}
	return EmptyPath();
}



// PortLand.
struct path *f(struct packet *pkt, struct map *env) {
	struct PG_instance *pgi = apply_parse_graph(pkt);
	struct entity *me = read_packet_inswitch(pgi);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pgi);
	value_t macDst = read_packet(pgi, "mac_dst");
	int sw_type = get_sw_type(dpid, in_port);		//get the type of current switch

	if(sw == 1) {					// "1" means edge switch and upstream
		if(pull_header(pgi, "IPv4", {}) == TRUE) {
			int dst_pod, dst_pos, dst_port;
			get_srp(macDst, &dst_pod, &dst_pos, &dst_port);		// calculate srp fields from macDst
			add_field(pgi, 8*14, 8*2, dst_pod);
			add_field(pgi, 8*16, 8*1, dst_pos);
			add_field(pgi, 8*17, 8*1, dst_port);
			mod_packet(pgi, "ethertype", 0x1234);
		}
		return CalcPath(in_port);		// Calculate path with in_port
	}
	else if(sw_type == 2)			// "2" means aggregate switch and upstream
		return CalcPath(in_port);

	else if(sw_type == 3) {			// "3" means core switch
		if(pull_header(pgi, "SRP", {}) == TRUE) {
			value_t podDst = read_packet(pgi, "dst_pod");
			return DirectPath(podDst);		// direct forward from port_id=podDst
		}
	}

	else if(sw_type == 4) {			// "4" means aggregate switch and downstream
		if(pull_header(pgi, "SRP", {}) == TRUE) {
			value_t posDst = read_packet(pgi, "dst_pos");
			return DirectPath(posDst);
		}
	}

	else if(sw_type == 5) {			// "5" means edge switch and downstream
		if(pull_header(pgi, "SRP", {}) == TRUE) {
			value_t portDst = read_packet(pgi, "dst_port");
			del_field(pgi, 8*17, 8*1);
			del_field(pgi, 8*16, 8*1);
			del_field(pgi, 8*14, 8*2);
			mod_packet(pgi, "ethertype", 0x0800);
			return DirectPath(portDst);
		}
	}
	return EmptyPath();			// Drop
}