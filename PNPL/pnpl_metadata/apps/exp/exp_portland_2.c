#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

int get_sw_type(int dpid, int in_port) {
	if(dpid<=8) {
		if(in_port <=4)
			return 1;	//edge交换机收到上行的包
		else if(in_port == 5||in_port==6)
			return 5;	//edge交换机收到下行的包
	}
	else if(dpid>8 && dpid<=16) {
		if(in_port == 1||in_port==2)
			return 2;	//Aggregate交换机收到上行的包
		else if(in_port == 3||in_port==4)
			return 4;	//Aggregate交换机收到下行的包
	}
	else if(dpid>16) {
		return 3;	//Core交换机收到上行的包
	}
	return 0;
}

void get_four(uint64_t dst_mac, uint16_t *pod, uint8_t *pos, uint8_t *port, uint16_t *vmid) {
	uint64_t temp = 0x000000000000;
	uint64_t res = dst_mac - temp;
	printf("CCCCCCCC res:%d\n", res);
	if(res>0 && res<=32)
		*pod = (uint16_t)((res-1)/8 + 1);
	else {
		*pod=0;
		return;
	}
	if((res%8)>0 && (res%8)<=4) *pos = 1;
	else *pos = 2;
	if(res % 4 == 0) *port = 4;
	else *port = res % 4;
	*vmid = 0;
	return;
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

	int sw_type = get_sw_type(dpid, in_port);
	if(sw_type == 0) return route();		// Otherwise.
	else if(sw_type == 1) {					//edge交换机收到上行的包
		if(dl_type == 0x0800) {
			uint64_t dl_dst = value_to_48(read_packet(pkt, "dl_dst"));
			uint16_t dst_pod=0;
			uint8_t dst_pos=0;
			uint8_t dst_port=0;
			uint16_t dst_vmid=0;
			get_four(dl_dst, &dst_pod, &dst_pos, &dst_port, &dst_vmid);
			if(dst_pod == 0) {
				printf("invalid MAC Address:%x\n", dl_dst);
				return route();
			}
			printf("pod is:%d, pos is:%d, port is:%d, vmid is:%d\n", dst_pod, dst_pos, dst_port, dst_vmid);
			add_field(pkt, 8*14, 8*2, value_from_16(dst_pod));
			add_field(pkt, 8*16, 8*1, value_from_8(dst_pos));
			add_field(pkt, 8*17, 8*1, value_from_8(dst_port));
			add_field(pkt, 8*18, 8*2, value_from_16(dst_vmid));
			mod_packet(pkt, "dl_type", value_from_16(0x1234));
		}
		if(in_port <= 2) return forward(me, in_port, 5);
		else return forward(me, in_port, 6);
	}
	else if(sw_type==2) {					//Aggregate交换机收到上行的包
		if(in_port == 1) return forward(me, in_port, 3);
		else return forward(me, in_port, 4);
	}
	else if(sw_type==3) {					//Core交换机收到上行的包
		if(dl_type == 0x1234) {
			pull_header(pkt);
			uint16_t dst_pod = value_to_16(read_packet(pkt, "dst_pod"));
			push_header(pkt);
			return forward(me, in_port, (int)dst_pod);
		}
		else return route();
	}
	else if(sw_type==4) {
		if(dl_type == 0x1234) {
			pull_header(pkt);
			uint8_t dst_pos = value_to_8(read_packet(pkt, "dst_pos"));
			push_header(pkt);
			return forward(me, in_port, (int)dst_pos);
		}
		else return route();
	}
	else {
		if(dl_type == 0x1234) {
			pull_header(pkt);
			uint8_t dst_port = value_to_8(read_packet(pkt, "dst_port"));
			push_header(pkt);
			del_field(pkt, 8*18, 8*2);
			del_field(pkt, 8*17, 8*1);
			del_field(pkt, 8*16, 8*1);
			del_field(pkt, 8*14, 8*2);
			mod_packet(pkt, "dl_type", value_from_16(0x0800));
			return forward(me, in_port, (int)dst_port);
		}
		else return route();
	}
	return route();
}
