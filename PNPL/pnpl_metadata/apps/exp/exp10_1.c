#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);
	uint16_t port_meta = 0;
	if(in_port <= 10)
		port_meta = 1;
	else if(in_port <= 20)
		port_meta = 2;
	else if(in_port <= 30)
		port_meta = 3;
	else if(in_port <= 40)
		port_meta = 4;
	else if(in_port <= 50)
		port_meta = 5;
	else if(in_port <= 60)
		port_meta = 6;
	else if(in_port <= 70)
		port_meta = 7;
	else if(in_port <= 80)
		port_meta = 8;
	else if(in_port <= 90)
		port_meta = 9;
	else
		port_meta = 10;
	value_t port_metadata = write_metadata_from_user(pkt, "ppp", 16, value_from_8(port_meta));
	uint64_t src = value_to_48(read_packet(pkt, "dl_src"));
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

	if(dl_type == 0x0800) {
		pull_header(pkt);
		read_metadata_from_user("ppp", &port_metadata);
		uint32_t d_ip=value_to_32(read_packet(pkt,"nw_dst"));
		//uint32_t ipn=htonl(inet_addr("11.0.0.0"));
		int output_port=port_meta;
		push_header(pkt);
		if(d_ip != 0)
			return forward(me, in_port, output_port);
		else return route();
	}
	return route();
}
