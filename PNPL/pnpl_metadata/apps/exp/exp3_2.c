#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));
	if(dl_type == 0x0800) {
		pull_header(pkt);
		uint32_t nw_dst = value_to_32(read_packet(pkt, "nw_dst"));
		push_header(pkt);
		if(nw_dst == 0x0b000001 || nw_dst == 0x0b000011 || nw_dst == 0x0b000021)
			return route();
		else return forward(me, in_port, in_port+1);
	}
	else if(dl_type == 0x8888) {
		pull_header(pkt);
		uint16_t vl_type1 = value_to_16(read_packet(pkt, "vl_type1"));
		if(vl_type1 == 0x0800) {
			pull_header(pkt);
			uint32_t nw_dst = value_to_32(read_packet(pkt, "nw_dst"));
			push_header(pkt);
			push_header(pkt);
			if(nw_dst == 0x0b000001 || nw_dst == 0x0b000011 || nw_dst == 0x0b000021)
				return route();
			else return forward(me, in_port, in_port+1);
		}
		else if(vl_type1 == 0x8889) {
			pull_header(pkt);
			uint16_t vl_type2 = value_to_16(read_packet(pkt, "vl_type2"));
			if(vl_type2 == 0x0800) {
				pull_header(pkt);
				uint32_t nw_dst = value_to_32(read_packet(pkt, "nw_dst"));
				push_header(pkt);
				push_header(pkt);
				push_header(pkt);
				if(nw_dst == 0x0b000001 || nw_dst == 0x0b000011 || nw_dst == 0x0b000021)
					return route();
				else return forward(me, in_port, in_port+1);
			}
		}
	}
	return route();
}
