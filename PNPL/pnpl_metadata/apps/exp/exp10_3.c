#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));
	uint16_t vl_type1;
	if(dl_type == 0x0800) {
		pull_header(pkt);
	}
	else if(dl_type == 0x8888) {
		pull_header(pkt);
		vl_type1 = value_to_16(read_packet(pkt, "vl_type1"));
		if(vl_type1 == 0x0800) {
			pull_header(pkt);

		}
		else if(vl_type1 == 0x8889) {
			pull_header(pkt);
			pull_header(pkt);
		}
	}

	int temp=0;
	int out_port=0;
	if(test_equal(pkt, "nw_proto", value_from_8(0x06))) {
		value_t metadata_temp=write_metadata(pkt,"nw_src");
		//value_t metadata_temp = write_metadata_from_user(pkt, "temp", 8, value_from_8(temp));
		pull_header(pkt);
		//read_metadata("nw_src",(&metadata_temp));
		//uint32_t s_ip=value_to_32(metadata_temp);
		//read_metadata_from_user("temp", &metadata_temp);
		//uint16_t tp_dst = value_to_16(read_packet(pkt, "tp_dst"));
		if(test_equal(pkt, "tp_dst", value_from_16(0x16))) {
			read_metadata("nw_src",(&metadata_temp));
			uint32_t s_ip=value_to_32(metadata_temp);
			if(s_ip == 0x0b000001)
				out_port = in_port+1;
			else out_port=0;
		}
		else {
			out_port=in_port+2;
			read_metadata("nw_src",(&metadata_temp));
			uint32_t s_ip=value_to_32(metadata_temp);
		}
		push_header(pkt);
	}

	if(dl_type == 0x0800) {
		push_header(pkt);
	}
	else if(dl_type == 0x8888) {
		push_header(pkt);
		if(vl_type1 == 0x0800) {
			push_header(pkt);
		}
		else if(vl_type1 == 0x8889) {
			push_header(pkt);
			push_header(pkt);
		}
	}

	if(out_port == 0) return route();
	else return forward(me, in_port, out_port);
}
