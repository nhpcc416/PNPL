#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);

	pull_header(pkt);

	uint32_t src_ip;
	if(strcmp(read_header_type(pkt), "ipv4") == 0) {
		src_ip=value_to_32(read_packet(pkt,"nw_src"));
		if(src_ip == 0x0a000001){
			push_header(pkt);
			return forward(me, in_port, 2);
		}
		else if(src_ip == 0x0a000002){
			push_header(pkt);
			return forward(me, in_port, 1);
		}
	}
	push_header(pkt);
	return route();

}
