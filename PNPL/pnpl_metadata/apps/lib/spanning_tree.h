#ifndef _SPANNING_TREE_H_
#define _SPANNING_TREE_H_

struct entity;
struct route;
struct nodeinfo;

struct nodeinfo *get_tree(struct entity *src, int src_port, struct entity *dst,
			  struct entity **switches, int switches_num);

struct route *get_route(struct entity *dst, int dst_port,
			struct nodeinfo *visited,
			struct entity **switches, int switches_num);

struct out_port *init_out_port();

void add_out_port(struct out_port *op, int e);

void print_out_port(struct out_port *op);

struct out_port *get_out_port(struct entity *dst, int dst_port,
			struct nodeinfo *visited,
			struct entity **switches, int switches_num);

#endif /* _SPANNING_TREE_H_ */
