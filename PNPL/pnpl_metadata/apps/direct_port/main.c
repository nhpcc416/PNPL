#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	if(dpid == 1) {
		update_checksum(pkt);
		if(in_port == 1)
			return forward(me, in_port, 2);
		else if(in_port == 2)
			return forward(me, in_port, 1);
	}
	else if(dpid == 2) {
		if(in_port == 1)
			return forward(me, in_port, 2);
		else if(in_port == 2)
			return forward(me, in_port, 1);
	}
	else if(dpid == 3) {
		if(in_port == 1)
			return forward(me, in_port, 2);
		else if(in_port == 2)
			return forward(me, in_port, 1);
	}
	else if(dpid == 4) {
		if(in_port == 1)
			return forward(me, in_port, 2);
		else if(in_port == 2)
			return forward(me, in_port, 1);
	}
	return route();
}

/*struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	int in_port = read_packet_inport(pkt);
	if(in_port == 1)
		return forward(me, in_port, 2);
	else if(in_port == 2)
		return forward(me, in_port, 1);
	else return route();
}*/

