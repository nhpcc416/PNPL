#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);

	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));
	uint32_t d_ip = 0;
	if(dl_type == 0x0800) {
		pull_header(pkt);
		d_ip=value_to_32(read_packet(pkt,"nw_dst"));
		push_header(pkt);
	}
	if(dpid == 4) {
		if(d_ip == 0xc0a80065)
			return forward(me, in_port, 1);
		else if(d_ip == 0xc0a80066)
			return forward(me, in_port, 2);
		else if(d_ip == 0xc0a80067)
			return forward(me, in_port, 3);
	}
	else if(dpid == 1) {
		if(d_ip == 0xc0a80065)
			return forward(me, in_port, 2);
		else if(d_ip == 0xc0a80066 || d_ip == 0xc0a80067)
			return forward(me, in_port, 1);
	}
	else if(dpid == 2 || dpid == 3) {
		if(d_ip == 0xc0a80065)
			return forward(me, in_port, 1);
		else if(d_ip == 0xc0a80066 || d_ip == 0xc0a80067)
			return forward(me, in_port, 2);
	}
	return route();
}

