#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

int pktnum = 0;

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);

	uint64_t src = value_to_48(read_packet(pkt, "dl_src"));
	uint64_t src_base=0xffffffffff00;
	printf ("dl_src:%x\n",src);

	uint32_t d_ip;
	uint16_t mac_type;
	mac_type = value_to_16(read_packet(pkt, "dl_type"));
	if(mac_type == 0x0800) {
		d_ip=value_to_32(read_packet(pkt,"nw_dst"));
		uint32_t ipn=htonl(inet_addr("10.0.0.0"));
		printf ("Dst Ip:%08x : %08x\n",d_ip,ipn);
		int output_port = d_ip-ipn;
		pktnum++;
		printf("pktnum is:%d\n", pktnum);
		return forward(me, in_port, 2);
	}
	return route();
}
