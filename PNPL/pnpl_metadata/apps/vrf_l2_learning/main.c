#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	//dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);




	uint64_t src = value_to_48(read_packet(pkt, "dl_src"));
	uint64_t src_base=0xffffffffff00;

	if(in_port==2) {
			printf("port 2222222222222");
			return forward(me, in_port, 1);
	}

	uint8_t vrf=-1;
	//printf ("\nSRC:%x\n",src);
	//printf ("0xff ff ff ff ff 00:%x\n",src_base);
	if (src-src_base<=10 && src-src_base>0){
		printf ("*******************vrf0***********************\n");
		vrf=0;
	}
	else if (src-src_base<=20 && src-src_base>10){
		printf ("*******************vrf1***********************\n");
		vrf=1;
	}
	else if (src-src_base<=30 && src-src_base>20){
		printf ("*******************vrf2***********************\n");
		vrf=2;
	}
	else if (src-src_base<=40 && src-src_base>30){
		printf ("*******************vrf3***********************\n");
		vrf=3;
	}
	else if (src-src_base<=50 && src-src_base>40){
		printf ("*******************vrf4***********************\n");
		vrf=4;
	}
	else if (src-src_base<=60 && src-src_base>50){
		printf ("*******************vrf5***********************\n");
		vrf=5;
	}
	else if (src-src_base<=70 && src-src_base>60){
		printf ("*******************vrf6***********************\n");
		vrf=6;
	}
	else if (src-src_base<=80 && src-src_base>70){
		printf ("*******************vrf7***********************\n");
		vrf=7;
	}
	else if (src-src_base<=90 && src-src_base>80){
		printf ("*******************vrf8***********************\n");
		vrf=8;
	}
	else if (src-src_base<=100 && src-src_base>90){
		printf ("*******************vrf9***********************\n");
		vrf=9;
	}
	value_t metadata_vrf = write_metadata_from_user(pkt, "vrf", 8, value_from_8(vrf));
	pull_header(pkt);

	uint32_t d_ip;
	if(strcmp(read_header_type(pkt), "ipv4") == 0) {
		read_metadata_from_user("vrf", &metadata_vrf);
		d_ip=value_to_32(read_packet(pkt,"nw_dst"));
		uint32_t ipn=htonl(inet_addr("10.0.0.0"));
		//printf ("Dst Ip:%08x : %08x\n",d_ip,ipn);
		//int output_port=vrf*(d_ip-ipn);
		int output_port=2;
		push_header(pkt);
		return forward(me, in_port, output_port);
	}
	push_header(pkt);
	return route();

}
