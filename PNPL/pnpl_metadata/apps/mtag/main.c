#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "xlog/xlog.h"
#include "types.h"
#include "pop_api.h"
#include "route.h"
#include "spanning_tree.h"
#include "mtag.h"

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

static struct route *handle_ipv4_unicast(uint32_t hsrc_ip, uint32_t hdst_ip, struct map *env)
{
	int switches_num;
	struct entity **switches = get_switches(&switches_num);
	struct route *r;
	struct entity *hsrc, *hdst;
	struct entity *src, *dst;
	int src_port, dst_port;
	struct nodeinfo *visited;

	hsrc = get_host_by_paddr(hsrc_ip);
	hdst = get_host_by_paddr(hdst_ip);
	if(hsrc == NULL || hdst == NULL) {
		xerror("ipv4_unicast: bad address.\n");
		return route();
	}
	src = get_host_adj_switch(hsrc, &src_port);
	dst = get_host_adj_switch(hdst, &dst_port);

	visited = get_tree(src, src_port, dst, switches, switches_num);
	r = get_route(dst, dst_port, visited, switches, switches_num);
	free(visited);
	return r;
}

static bool is_multicast_ip(uint32_t ip)
{
	if((ip >> 24) >= 224 && (ip >> 24) < 240)
		return true;
	return false;
}

struct route *f(struct packet *pkt, struct map *env)
{
	int switches_num;
	struct entity **switches = get_switches(&switches_num);
	struct entity *hsrc, *hdst;
	uint32_t hsrc_ip, hdst_ip;
	struct entity *src, *dst;
	int src_port, dst_port;
	struct nodeinfo *visited;
	struct out_port *op;

	haddr_t src_haddr, dst_haddr;
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	uint8_t up1, up2, down1, down2;
	uint16_t mac_type;
	int i;

	dst_haddr = value_to_haddr(read_packet(pkt, "dl_dst"));
	src_haddr = value_to_haddr(read_packet(pkt, "dl_src"));
	mac_type = value_to_16(read_packet(pkt, "dl_type"));
	printf("mac_type is:%x.\n", mac_type);
	printf("src_haddr is: ");
	for(i=0;i<6;i++)
		printf("%x:",src_haddr.octet[i]);
	printf("\n");
	printf("dst_haddr is: ");
	for(i=0;i<6;i++)
		printf("%x:",dst_haddr.octet[i]);
	printf("\n");
	pull_header(pkt);

	printf("before pull to mtag.\n");
	if(strcmp(read_header_type(pkt), "mtag") == 0)
	{
		printf("go to mtag.\n");
		up1 = value_to_8(read_packet(pkt, "up1"));
		up2 = value_to_8(read_packet(pkt, "up2"));
		down1 = value_to_8(read_packet(pkt, "down1"));
		down2 = value_to_8(read_packet(pkt, "down2"));
		printf("up1:%d,up2:%d,down1:%d,down2:%d.\n", up1, up2, down1, down2);
		printf("before judge.\n");
		if((up1 == 0xff)&&(up2 == 0xff)&&(down1 == 0xff)&&(down2 == 0xff))
		{
			printf("go to judge1.\n");
			//op = init_out_port();
			hsrc = get_host_by_haddr(src_haddr);
			hdst = get_host_by_haddr(dst_haddr);

			printf("after get_host_by_haddr.\n");
			if(hsrc == NULL || hdst == NULL) {
				printf("error, hsrc or hdst is null.\n");
				push_header(pkt);
				return route();
			}
			src = get_host_adj_switch(hsrc, &src_port);
			dst = get_host_adj_switch(hdst, &dst_port);
			printf("after get_host_adj_switch.\n");
			visited = get_tree(src, src_port, dst, switches, switches_num);
			printf("after get_tree.\n");
			//struct out_port *op = init_out_port();
			op = get_out_port(dst, dst_port, visited, switches, switches_num);
			printf("after get_out_port.\n");
			print_out_port(op);
			printf("after print_out_port.\n");
			assert(op->port_num == 5);
			mod_packet(pkt, "up1", value_from_8(op->port[3]));
			mod_packet(pkt, "up2", value_from_8(op->port[2]));
			mod_packet(pkt, "down1", value_from_8(op->port[1]));
			mod_packet(pkt, "down2", value_from_8(op->port[0]));
			/*mod_packet(pkt, "up1", value_from_8(4));
			mod_packet(pkt, "up2", value_from_8(3));
			mod_packet(pkt, "down1", value_from_8(1));
			mod_packet(pkt, "down2", value_from_8(1));*/
			push_header(pkt);
			return forward(me, in_port, op->port[4]);
		}
		else if(up1 != 0xff){
			mod_packet(pkt, "up1", value_from_8(0xff));
			push_header(pkt);
			return forward(me, in_port, up1);
		}
		else if((up1 == 0xff)&&(up2 != 0xff))
		{
			mod_packet(pkt, "up2", value_from_8(0xff));
			push_header(pkt);
			return forward(me, in_port, up2);
		}
		else if((up1 == 0xff)&&(up2 == 0xff)&&(down1 != 0xff))
		{
			mod_packet(pkt, "down1", value_from_8(0xff));
			push_header(pkt);
			return forward(me, in_port, down1);
		}
		else if((up1 == 0xff)&&(up2 == 0xff)&&(down1 == 0xff)&&(down2 != 0xff))
		{
			push_header(pkt);
			return forward(me, in_port, down2);
		}
	}
	else if(strcmp(read_header_type(pkt), "ipv4") == 0)
	{
		hsrc_ip = value_to_32(read_packet(pkt, "nw_src"));
		hdst_ip = value_to_32(read_packet(pkt, "nw_dst"));
		if(is_multicast_ip(hdst_ip)) {
			printf("error, ipv4 multicast.\n");
			return route();
		} else {
			struct route *temp = NULL;
			temp = handle_ipv4_unicast(hsrc_ip, hdst_ip, env);
			push_header(pkt);
			return temp;
		}
	}
	else
	{
		printf("error, not mtag or ipv4.\n");
		push_header(pkt);
		return route();
	}
}
