#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	//dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	//uint64_t src = value_to_48(read_packet(pkt, "dl_src"));

	if(strcmp(read_header_type(pkt), "ipv4") == 0) {
		uint8_t ip_proto=value_to_8(read_packet(pkt,"nw_proto"));
		if(ip_proto == 0x06) {
			pull_header(pkt);
			int output_port=3;
			uint16_t tp_dst=value_to_16(read_packet(pkt,"tp_dst"));
			if(tp_dst == 0x0050)
				output_port = 4;
			else if(tp_dst == 0x01bb)
				output_port = 5;
			push_header(pkt);
			if(output_port != 3)
				return forward(me, in_port, output_port);
			else
				return route();
		}
		return forward(me, in_port, 2);
	}
	return forward(me, in_port, 2);
}
