#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	//dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);
	uint64_t src = value_to_48(read_packet(pkt, "dl_src"));

	if(in_port == 1) {
		printf("before add portland.\n");
		add_header(pkt, "portland");
		printf("after add portland.\n");
		mod_packet(pkt, "dl_type", value_from_16(0x1234));
		pull_header(pkt);
		uint16_t pl1=value_to_16(read_packet(pkt,"pl1"));
		push_header(pkt);
		return forward(me, in_port, 2);
	}
	else {
		pull_header(pkt);
		del_field(pkt, 8*6, 8*2);
		del_field(pkt, 8*4, 8*2);
		del_field(pkt, 8*2, 8*2);
		del_field(pkt, 0, 8*2);
		push_header(pkt);
		return forward(me, in_port, 1);
	}
	return route();
}
