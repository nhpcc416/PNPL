#include "xlog/xlog.h"
#include "pop_api.h"
#include "learning.h"
#include "route.h"
#include <stdio.h>

static struct route *forward(struct entity *esw, int in_port, int out_port)
{
	struct route *r = route();
	route_add_edge(r, edge(NULL, 0, esw, in_port));
	route_add_edge(r, edge(esw, out_port, NULL, 0));
	return r;
}


void init_f(struct map *env)
{
	xinfo("f init\n");
}

int get_type_switch(int dpid, int in_port) {
	if(dpid==1||dpid==2||dpid==5||dpid==6||dpid==9||dpid==10||dpid==13||dpid==14) {
		if(in_port == 1||in_port==2)
			return 1;	//edge交换机收到上行的包
		else if(in_port == 3||in_port==4)
			return 5;	//edge交换机收到下行的包
	}
	else if(dpid==3||dpid==4||dpid==7||dpid==8||dpid==11||dpid==12||dpid==15||dpid==16) {
		if(in_port == 1||in_port==2)
			return 2;	//Aggregate交换机收到上行的包
		else if(in_port == 3||in_port==4)
			return 4;	//Aggregate交换机收到下行的包
	}
	else if(dpid>16) {
		return 3;	//Core交换机收到上行的包
	}
	return 0;
}
struct route *f(struct packet *pkt, struct map *env)
{
	struct entity *me = read_packet_inswitch(pkt);
	dpid_t dpid = get_switch_dpid(me);
	int in_port = read_packet_inport(pkt);

	//uint64_t src = value_to_48(read_packet(pkt, "dl_src"));
	uint16_t dl_type = value_to_16(read_packet(pkt, "dl_type"));

	int sw_type = get_type_switch(dpid, in_port);
	if(sw_type == 0) return route();
	if(sw_type==1) {
		if(in_port == 1) return forward(me, in_port, 3);
		else return forward(me, in_port, 4);
	}
	else if(sw_type==2) {
		if(in_port == 1) return forward(me, in_port, 3);
		else return forward(me, in_port, 4);
	}
	else {
		if(dl_type == 0x1234) {
			pull_header(pkt);	// goto portland
			if(sw_type==3) {
				uint16_t dst_pod = value_to_16(read_packet(pkt, "dst_pod"));
				push_header(pkt);
				return forward(me, in_port, (int)dst_pod);
			}
			else if(sw_type==4) {
				uint8_t dst_pos = value_to_8(read_packet(pkt, "dst_pos"));
				push_header(pkt);
				return forward(me, in_port, (int)dst_pos);
			}
			else {
				uint8_t dst_port = value_to_8(read_packet(pkt, "dst_port"));
				push_header(pkt);
				return forward(me, in_port, (int)dst_port);
			}
		}
	}
	return route();
}
