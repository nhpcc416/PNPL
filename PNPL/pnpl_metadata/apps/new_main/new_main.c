#ifndef _IGMP_H_
#define _IGMP_H_

#include "pop_api.h"
#include "types.h"
#include "value.h"
#include <assert.h>
#include <string.h>
#include <arpa/inet.h> //ntohl, ntohs, htonl, htons

#include <stdlib.h>
#include "xlog/xlog.h"
#include "route.h"
#include "spanning_tree.h"
#include "map.h"

#include <stdio.h>

/* IGMP 包长度的最大值*/

#define IGMP_PROTOCOL_NUM 	2

/*IGMP packet version type */
#define QUERY_REPORT 	 		0x11 	//v1 v2
#define V1_MEMBERSHIP_REPORT 	0x12
#define V2_MEMBERSHIP_REPORT 	0x16
#define V2_LEAVE_GROUP 			0x17
#define V3_MEMBERSHIP_REPORT 	0x22

/* IGMP v3 membership report record type */
#define V3_MODE_IS_INCLUDE 		1
#define V3_MODE_IS_EXCLUDE 		2
#define V3_CHANGE_TO_INCLUDE 	3
#define V3_CHANGE_TO_EXCLUDE 	4
#define V3_ALLOW_NEW_SOURCE 	5
#define V3_BLOCK_OLD_SOURCE 	6

/* query interval seconds */
#define QUERY_INTERVAL 125
#define QUERY_RESPONSE_INTERVAL 100 //10s, 100 * 0.1s

#define IGMP_ALL_HOSTS_MAC  	(0xE0000001L) //224.0.0.1
#define IGMP_ALL_ROUTERS_MAC  	(0xE0000002L) //224.0.0.2
#define IGMPV3_ALL_MAR		  	(0xE0000016L) //224.0.0.22
#define IGMP_LOCAL_GROUP	  	(0xE0000000L) //224.0.0.0
#define IGMP_LOCAL_GOURP_MASK  	(0xFFFFFF00L) //255.255.255.0

#define MAX_PACKET_SIZE 1480 	//1500 - 20
// #define PACKET_SIZE 8	//v2, v1 packet size
#define IGMP_HEADER_LEN 8 	//V1, V2
#define IGMP_V3_QUERY_HEADER_LEN 12
#define IGMP_V3_RECORD_HEADER_LEN 8
#define IGMP_V3_REPORT_HEADER_LEN 12

#define RECORD_NAME "igmp_record"

/*IGMP v1,v2 header */
struct igmphdr{
	uint8_t type; 	//packet type 0x11 0x12 0x16 0x17
	uint8_t max_resp_code;
	uint16_t csum;
	uint32_t groupid;  //224.0.0.1在内存中为：（从低地址到高地址）0x01, 0x00, 0x00, 0xea
};
/* IGMP v3  group record*/
struct igmpv3_grec{
	uint8_t grec_type; 	 	//record type
	uint8_t grec_auxdlen;
	uint16_t grec_nsrcs;	//num of source adresses
	uint32_t grec_mcaddr; 	//multicast address, 224.0.0.1在内存中为：（从低地址到高地址）0x01, 0x00, 0x00, 0xea
	uint32_t grec_src[0]; 	//source addresses IP地址：10.0.0.1在内存中为：（从低地址到高地址）0x01, 0x00, 0x00, 0x0a
	/*紧随grec_auxlen个source adress， 但是协议的auxiliary data就在source adress之后，
	 * 只能在结构体之外体现了。
	*/
};
/* IGMP v3 membership report packet*/
struct igmpv3_report{
	uint8_t type ; 		//report type 0x22
	uint8_t max_rsp_code;
	uint16_t csum;
	uint16_t resv; 	//reserved
	uint16_t ngrecord;	//number of group records
	struct igmpv3_grec grec[0]; //group records
};

/*IGMP v3 membership query packet.(not used now)*/
struct igmpv3_query{
	uint8_t type ;  	//report type 0x11
	uint8_t max_rsp_code;
	uint16_t csum;
	uint32_t groupid;
	uint8_t resv_s_qrv; //4bit resv, 1bit S, 3bit QRV
	uint8_t qqic;
	uint16_t nsrcs; 	//number of source address
	uint32_t src[0];
};

struct igmp_addrs {
	int n;
	uint32_t *addrs;
};

struct map;

struct map *igmp_init(void);
struct igmp_addrs *igmp_get_maddrs(struct map *group_table, uint32_t groupid);
void process_igmp(struct map *group_table, uint32_t src_ip, const uint8_t *buffer, int len);

#endif  /*_IGMP_H_*/

/*** group table ***/
static bool mgt_eq(int_or_ptr_t k1, int_or_ptr_t k2)
{
	return k1.v == k2.v;
}

static unsigned int mgt_hash(int_or_ptr_t key)
{
	return key.v;
}

static int_or_ptr_t mgt_dup_key(int_or_ptr_t key)
{
	return key;
}

static void mgt_free_key(int_or_ptr_t key)
{

}

static bool mgt_eq_val(int_or_ptr_t k1, int_or_ptr_t k2)
{
	return false;
}

static void mgt_free_val(int_or_ptr_t val)
{

}

struct map *igmp_init(void)
{
	return map(mgt_eq, mgt_hash, mgt_dup_key, mgt_free_key);
}

struct igmp_addrs *igmp_get_maddrs(struct map *group_table, uint32_t groupid)
{
	struct igmp_addrs *l;

	l = map_read(group_table, INT(groupid)).p;
	if(l == NULL) {
		l = malloc(sizeof(struct igmp_addrs));
		l->addrs = NULL;
		l->n = 0;
		map_add_key(group_table, INT(groupid), PTR(l), mgt_eq_val, mgt_free_val);
	}
	return l;
}

static bool addrs_in(struct igmp_addrs *l, uint32_t ip)
{
	int i;
	for(i = 0; i < l->n; i++)
		if(l->addrs[i] == ip)
			return true;
	return false;
}

static void addrs_add(struct igmp_addrs *l, uint32_t ip)
{
	if(addrs_in(l, ip))
		return;

	l->addrs = realloc(l->addrs, l->n + 1);
	l->addrs[l->n] = ip;
	l->n++;
}

static void addrs_del(struct igmp_addrs *l, uint32_t ip)
{
	int i;
	for(i = 0; i < l->n; i++)
		if(l->addrs[i] == ip) {
			int j;
			l->n--;
			for(j = i; j < l->n; j++)
				l->addrs[j] = l->addrs[j+1];
			return;
		}
}

static void enter_group(struct map *gt, uint32_t groupid, uint32_t ip)
{
	struct igmp_addrs *l;
	l = igmp_get_maddrs(gt, groupid);
	addrs_add(l, ip);
	map_mod(gt, INT(groupid), PTR(l));
}

static void exit_group(struct map *gt, uint32_t groupid, uint32_t ip)
{
	struct igmp_addrs *l;
	l = igmp_get_maddrs(gt, groupid);
	addrs_del(l, ip);
	map_mod(gt, INT(groupid), PTR(l));
}

/*checksum calculate, return the host order*/
uint16_t checksum(const uint8_t *buff, int len){
	int count = len >> 1;
	const uint16_t *data = (const uint16_t *)buff;
	uint32_t csum = 0;

	int i;
	for(i = 0; i < count; i++)
		csum += ntohs(*(data + i));

	if(len & 0x01){
		uint32_t tmp = data[len - 1] << 8;
		csum += tmp;
	}
	uint16_t high = (csum & 0xffff0000 ) >> 16;
	uint16_t low = csum & 0xffff;

	return ~(low + high);
}

void process_igmp(struct map *group_table, uint32_t src_ip, const uint8_t *buffer, int len)
{
	if( checksum(buffer, len) != 0)
		return;
	if(len == IGMP_HEADER_LEN){ 	//igmp v1, v2`
		const struct igmphdr *igmp = (const struct igmphdr *) buffer;
		uint32_t groupid = igmp->groupid;

		switch( igmp->type ){
			case QUERY_REPORT:
				/*controller cannot receive query report packet*/
				break;
			case V1_MEMBERSHIP_REPORT:
				/*same as V2_MEMBERSHIP_REPORT...*/
			case V2_MEMBERSHIP_REPORT:
				enter_group(group_table, groupid, src_ip);
				break;
			case V2_LEAVE_GROUP:
				exit_group(group_table, groupid, src_ip);
				break;
			default:
				/*error*/
				break;
		}
	}else if(len >= IGMP_V3_REPORT_HEADER_LEN){ //igmp v3
		const struct igmpv3_report *igmpv3_pkt = (const struct igmpv3_report *) buffer;
		/*only deal with membership report*/
		if(igmpv3_pkt->type != V3_MEMBERSHIP_REPORT)
			return;
		uint16_t ngrecord = ntohs(igmpv3_pkt->ngrecord);
		const uint8_t *grecord = (const uint8_t *) igmpv3_pkt->grec;
		int i;

		/*deal with each record*/
		for(i = 0; i < ngrecord; i++){
			uint8_t grec_type = *grecord++;
			uint8_t grec_auxdlen = *grecord++; //一般为0，不为0,则要跳过末尾的grec_auxdlen个字节
			uint16_t grec_nsrc = ntohs(*((const uint16_t *)grecord));
			grecord += sizeof(uint16_t);
			uint32_t grec_mcaddr = ntohl(*((const uint32_t *)grecord));
			grecord += sizeof(uint32_t);

			switch( grec_type ){
				case V3_MODE_IS_INCLUDE: 	//接收来自源，如果源为空则， 为退出报告，否则加入组
					if( grec_nsrc == 0){ //INCLUDE{}, 表示退出报告
						exit_group(group_table, grec_mcaddr, src_ip);
					}else{ //不为0，表示对源进行控制，现在不对源进行控制，只进行插入
						enter_group(group_table, grec_mcaddr, src_ip);
					}
					break;
				case V3_MODE_IS_EXCLUDE:
					if( grec_nsrc == 0){ //EXCLUDE{}, 表示组加入报告
						enter_group(group_table, grec_mcaddr, src_ip);
					}else{ //不为0，表示对源进行控制，现在不对源进行控制，只进行插入
						enter_group(group_table, grec_mcaddr, src_ip);
					}
					break;
				case V3_CHANGE_TO_INCLUDE: //源地址不为空，则当做插入，否则当做退出操作
					if( grec_nsrc == 0){
						exit_group(group_table, grec_mcaddr, src_ip);
					}else{
						enter_group(group_table, grec_mcaddr, src_ip);
					}
					break;
				case V3_CHANGE_TO_EXCLUDE: //暂时当做插入处理
					enter_group(group_table, grec_mcaddr, src_ip);
					break;
				case V3_ALLOW_NEW_SOURCE: //不处理
					break;
				case V3_BLOCK_OLD_SOURCE: //不处理
					break;
				default:
					/*error*/
					break;
			}
			grecord += sizeof(uint32_t) * grec_nsrc + grec_auxdlen; //step to next record
		}
	}else{
		/*error, drop */
		return;
	}
	return;
}

static struct route *handle_sdnp(struct packet *pkt, struct map *env)
{
	int switches_num;
	struct entity **switches = get_switches(&switches_num);
	struct route *r;
	struct entity *src, *dst;
	int src_port, dst_port;
	struct nodeinfo *visited;

	pull_header(pkt);
	src = get_switch(value_to_32(read_packet(pkt, "dpid_src")));
	dst = get_switch(value_to_32(read_packet(pkt, "dpid_dst")));
	src_port = value_to_16(read_packet(pkt, "port_src"));
	dst_port = value_to_16(read_packet(pkt, "port_dst"));

	if(src == NULL || dst == NULL) {
		xerror("sdnp: bad address.\n");
		return route();
	}

	visited = get_tree(src, src_port, dst, switches, switches_num);
	r = get_route(dst, dst_port, visited, switches, switches_num);
	free(visited);
	push_header(pkt);
	return r;
}

static struct route *handle_ipv4_unicast(uint32_t hsrc_ip, uint32_t hdst_ip, struct map *env)
{
	int switches_num;
	struct entity **switches = get_switches(&switches_num);
	struct route *r;
	struct entity *hsrc, *hdst;
	struct entity *src, *dst;
	int src_port, dst_port;
	struct nodeinfo *visited;

	hsrc = get_host_by_paddr(hsrc_ip);
	hdst = get_host_by_paddr(hdst_ip);
	if(hsrc == NULL || hdst == NULL) {
		xerror("ipv4_unicast: bad address.\n");
		return route();
	}
	src = get_host_adj_switch(hsrc, &src_port);
	dst = get_host_adj_switch(hdst, &dst_port);

	visited = get_tree(src, src_port, dst, switches, switches_num);
	r = get_route(dst, dst_port, visited, switches, switches_num);
	free(visited);
	return r;
}

static struct route *handle_ipv4_multicast(uint32_t hsrc_ip, uint32_t hdst_ip, struct map *env)
{
	int i;
	int switches_num;
	struct entity **switches = get_switches(&switches_num);
	struct route *rx, *r;
	struct entity *hsrc, *hdst;
	struct entity *src, *dst;
	int src_port, dst_port;
	struct nodeinfo *visited;

	struct igmp_addrs *l = igmp_get_maddrs(map_read(env, PTR("group_table")).p, hdst_ip);
	r = route();

	hsrc = get_host_by_paddr(hsrc_ip);
	if(hsrc == NULL) {
		xerror("ipv4_multicast: bad src address.\n");
		return route();
	}
	src = get_host_adj_switch(hsrc, &src_port);

	visited = get_tree(src, src_port, NULL, switches, switches_num);
	xinfo("group_id: %08x, group_n: %d\n", hdst_ip, l->n);
	for(i = 0; i < l->n; i++) {
		hdst = get_host_by_paddr(l->addrs[i]);
		if(hdst == NULL) {
			xerror("ipv4_multicast: bad dst address.\n");
			continue;
		}

		dst = get_host_adj_switch(hdst, &dst_port);
		rx = get_route(dst, dst_port, visited, switches, switches_num);
		route_union(r, rx);
		route_free(rx);
	}
	free(visited);
	return r;
}

static struct route *handle_igmp(struct packet *pkt, struct map *env)
{
	int len;
	uint32_t hsrc_ip = value_to_32(read_packet(pkt, "nw_src"));
	const uint8_t *payload = read_payload(pkt, &len);
	process_igmp(map_read(env, PTR("group_table")).p, hsrc_ip, payload, len);
	return route();
}

static bool is_multicast_ip(uint32_t ip)
{
	if((ip >> 24) >= 224 && (ip >> 24) < 240)
		return true;
	return false;
}

static struct route *handle_ipv4(struct packet *pkt, struct map *env)
{
	if(test_equal(pkt, "nw_proto", value_from_8(0x02))) {
		return handle_igmp(pkt, env);
	} else {
		uint32_t hsrc_ip = value_to_32(read_packet(pkt, "nw_src"));
		uint32_t hdst_ip = value_to_32(read_packet(pkt, "nw_dst"));
		if(is_multicast_ip(hdst_ip)) {
			return handle_ipv4_multicast(hsrc_ip, hdst_ip, env);
		} else {
			mod_packet(pkt, "ttl", value_from_8(42));
			return handle_ipv4_unicast(hsrc_ip, hdst_ip, env);
		}
	}
}

void init_f(struct map *env)
{
	xinfo("f init\n");
	map_add_key(env, PTR("group_table"), PTR(igmp_init()),
		    mapf_eq_map, mapf_free_map);
}

struct route *f(struct packet *pkt, struct map *env)
{
	struct route *r = NULL;

	/* inspect network header */
	pull_header(pkt);

	/* call handler */
	if (strcmp(read_header_type(pkt), "sdnp") == 0) {
		r = handle_sdnp(pkt, env);
	} else if(strcmp(read_header_type(pkt), "ipv4") == 0) {
		r = handle_ipv4(pkt, env);
	} else {
		xinfo("unknown protocol: %s.\n", read_header_type(pkt));
		r = route();
	}

	/* reset header */
	push_header(pkt);

	return r;
}
