#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include<time.h>

#include "xswitch-private.h"

#define ENTRY_LIVE_TIME 300

struct flow_table *flow_table(int tid, enum flow_table_type type, int size)
{
	int i;
	struct flow_table *ft;
	int sz = sizeof(unsigned long);
	int msize = (size + sz - 1) / sz;
	ft = malloc(sizeof(struct flow_table) + msize * sz);
	ft->tid = tid;
	ft->type = type;
	ft->size = size;
	ft->fields_num = 0;
	ft->entry_num = 0;
	ft->match_num = 0;
	for(i=0; i<size; i++)
	{
		ft->entry_ttl[i].pindex = NULL;
		ft->entry_ttl[i].ttl = 0;
		ft->has_match[i] = 0;
	}
	memset(ft->index_map, 0, msize * sz);
	return ft;
}

void flow_table_free(struct flow_table *ft)
{
	free(ft);
}

void flow_table_add_field(struct flow_table *ft,
			  const char *name, enum match_field_type type, int offset, int length)
{
	struct match_field *f = ft->fields + ft->fields_num;
	/* XXX: hack */
	if(strlen(name) >= 2 && name[0] == '_' && name[1] == '_')
		return;
	assert(ft->fields_num < FLOW_TABLE_NUM_FIELDS);
	strncpy(f->name, name, MATCH_FIELD_NAME_LEN);
	f->name[MATCH_FIELD_NAME_LEN - 1] = 0;
	f->type = type;
	f->offset = offset;
	f->length = length;
	ft->fields_num++;
}

int flow_table_get_field_index(struct flow_table *ft, const char *name)
{
	int i;
	for(i = 0; i < ft->fields_num; i++) {
		//printf("name is:%s,ft->fields[i].name is:%s\n",name,ft->fields[i].name);
		if(strcmp(name, ft->fields[i].name) == 0)
			return i;
	}
	return -1;
}

int flow_table_get_tid(struct flow_table *ft)
{
	return ft->tid;
}

int flow_table_get_entry_index(struct flow_table *ft)
{
	int i, j;
	int sz = sizeof(unsigned long);
	int m = (ft->size + sz - 1) / sz;
	for(i = 0; i < m; i++)
		if(ft->index_map[i] != ~(0ul))
			for(j = 0; j < sz; j++)
				if((ft->index_map[i] & (1 << j)) == 0) {
					ft->index_map[i] |= 1 << j;
					assert(i * sz + j < ft->size);
					return i * sz + j;
				}
	return -1;
}

void flow_table_put_entry_index(struct flow_table *ft, int index)
{
	int sz = sizeof(unsigned long);
	if(index < 0) {
		printf("index is:%d. no need to put.\n", index);
		return;
	}
	assert(index >= 0 && index < ft->size);
	ft->index_map[index / sz] &= ~(1 << (index % sz));
	*(ft->entry_ttl[index].pindex) = -1;
	ft->entry_ttl[index].ttl = 0;
	ft->entry_num--;
	if(ft->has_match[index] == 1)
	{
		ft->mm[index] = match();
		ft->has_match[index] = 0;
		ft->match_num--;
	}
}

//Curtis
int flow_table_entry_index_dispatcher(struct xswitch *sw, struct flow_table *ft)
{
	int temp = flow_table_get_entry_index(ft);
	int random_del;
	struct msgbuf *msg;
	if(temp == -1)
		printf("-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1.\n");
	if(temp != -1)
	{
		ft->entry_ttl[temp].ttl = time(NULL);
		ft->entry_num++;
		printf("init a entry. dpid:%d,tid:%d,index:%d,init time:%ld.\n", sw->dpid, ft->tid, temp, ft->entry_ttl[temp].ttl);
		return temp;
	}
	else
	{
		//srand((int)time(0));
		//srand((unsigned)time(NULL));
		random_del = rand()%(ft->size - 1)+1;
		printf("random_del is:%d\n",random_del);
		//random_del = 3;
		msg = msg_flow_entry_del(ft, random_del);
		xswitch_send(sw, msg);
		flow_table_put_entry_index(ft, random_del);
		printf("delete a entry because of full. dpid:%d,tid:%d,index:%d.\n", sw->dpid, ft->tid, random_del);
		temp = flow_table_get_entry_index(ft);
		//assert(temp < ft->size);
		assert(temp != -1);
		return temp;
	}
}

//Curtis add flow_table_timeout to support timeout.
void flow_table_timeout(struct xswitch *sw, struct flow_table *ft)
{
	int i;
	struct msgbuf *msg;
	time_t temp_t;
	while(1)
	{
		if(sw->dpid > 100)
		{
			break;
		}
		if(ft->entry_num > 0)
			for(i = 0; i < ft->size; i++)
			{
				if(ft->entry_ttl[i].ttl != 0)
				{
					temp_t = time(NULL);
					double diff = difftime(temp_t, ft->entry_ttl[i].ttl);
					//printf("dpid:%d,tid:%d,entry_num:%d,entry's index:%d,ttl:%f.\n",sw->dpid, ft->tid, ft->entry_num, i, diff);
					if(diff >= ENTRY_LIVE_TIME)
					{
						msg = msg_flow_entry_del(ft, i);
						xswitch_send(sw, msg);
						flow_table_put_entry_index(ft, i);
						printf("delete entry because of timeout. dpid:%d,tid:%d,index:%d,time(should be 0):%d.\n",sw->dpid, ft->tid, i, ft->entry_ttl[i].ttl);
					}
				}
			}
		sleep(1);
	}
	return;
}

//Curtis
void thread_flow_table_timeout(void *argu)
{
	struct argu_timeout *arg = (struct argu_timeout*)argu;
	struct xswitch *sw = arg->sw;
	struct flow_table *ft = arg->ft;
	printf("new thread. dpid = %d,tid = %d,thread id = %lu\n", sw->dpid, ft->tid, pthread_self());
	flow_table_timeout(sw, ft);
	printf("thread over.\n");
	pthread_exit("thread over.\n");
	return;
}

//Curtis
void create_thread_flow_table_timeout(struct xswitch *sw, struct flow_table *ft)
{
	pthread_attr_t attr;
	pthread_t id;
	int ret;
	static struct argu_timeout arg;
	arg.sw = sw;
	arg.ft = ft;
	if (pthread_attr_init(&attr) != 0)
	{
		printf ("pthread_attr_init error! the table id is:%d\n", ft->tid);
		return;
	}
	if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0)
	{
		printf ("pthread_attr_setdetachstate error! the table id is:%d\n", ft->tid);
		return;
	}
	ret = pthread_create(&id, &attr, (void *)thread_flow_table_timeout, (void *)&arg);
	if(ret != 0)
	{
	    printf ("Create pthread error! the table id is:%d\n", ft->tid);
	    return;
	}
	/*if (pthread_detach(id) != 0)
	{
		printf ("pthread_detach error! the table id is:%d\n", ft->tid);
		return;
	}*/
	usleep(10);
	return;
}

int match_field_compare(struct match_field f1,struct match_field f2){
	if (f1.length!=f2.length) return -1;
	//if (f1->metadata_offset!=f2->metadata_offset) return -1;
	if (f1.offset!=f2.offset) return -1;
	if (f1.type!=f2.type) return -1;
	if (strcmp(f1.name,f2.name)!=0) return -1;
	return 0;
}


int flow_table_compare(struct flow_table *t1,struct flow_table *t2){
	if (t1->fields_num!=t2->fields_num) return -1;
	if (t1->size !=t2->size) return -1;
	if (t1->type!=t2->type) return -1;
	int i ;
	for ( i=0;i<t1->fields_num;i++){
		if (match_field_compare(t1->fields[i],t2->fields[i])!=0)
			return -1;
	}
	return 0;
}
