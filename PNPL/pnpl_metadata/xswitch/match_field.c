#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "xlog/xlog.h"
#include "xswitch-private.h"

struct match_fields *match_fields(void)
{
	struct match_fields *a = malloc(sizeof(struct match_fields));
	a->fields_num = 0;
	return a;
}

/* milktank
void match_fields_collect(struct match_fields *m, const char *name)
{
	int n = m->fields_num;
	int i;
	if (n==0)
	{
		strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
		m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
		m->fields_num++;
	}
	else
	{
		for ( i=0;i<n;i++){
			if (strcmp(m->fields[n].name,name)!=0)
			{
				strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
				m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
				m->fields_num++;
			}
		}

	}
}
*/


//ck
void match_fields_collect(struct match_fields *m, const char *name, int type)
{
	int n = m->fields_num;
	int i;
	int flag = 0;
	if (n==0)
	{
		if(type == 0)
		{
			m->fields[n].metadata_type = FROM_PACKET;
		}
		else
		{
			m->fields[n].metadata_type = FROM_USER;
		}
		strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
		m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
		m->fields_num++;
		printf("match_fields_collect: Add the name: %s\n", name);
	}
	else
	{
		for ( i=0;i<n;i++){
			if (strcmp(m->fields[n].name,name) == 0)
			{
				flag = 1;
			}
		}
		if(flag == 0)
		{
			if(type == 0)
			{
				m->fields[n].metadata_type = FROM_PACKET;
			}
			else
			{
				m->fields[n].metadata_type = FROM_USER;
			}
			strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
			m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
			m->fields_num++;
			printf("match_fields_collect: Add the name: %s\n", name);
		}
		else{printf("match_fields_collect: already exists the RM name: %s", name);}
	}
}



void match_fields_add(struct match_fields *m, const char *name, int offset, int  length,struct header *spec)
{
	int n = m->fields_num;
	assert(n < FLOW_TABLE_NUM_FIELDS);
	strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
	m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
	m->fields[n].offset = offset;
	m->fields[n].length = length;
	m->fields[n].spec=spec;
	if (n==0)
		m->fields[n].metadata_offset=32;
	else
		m->fields[n].metadata_offset=m->fields[n-1].metadata_offset+m->fields[n-1].length;
	m->fields_num++;
}

//Curtis
void match_fields_add_from_user(struct match_fields *m, const char *name, int  length, value_t value, struct header *spec)
{
	int n = m->fields_num;
	assert(n < FLOW_TABLE_NUM_FIELDS);
	m->fields[n].metadata_type = FROM_USER;
	strncpy(m->fields[n].name, name, MATCH_FIELD_NAME_LEN);
	m->fields[n].name[MATCH_FIELD_NAME_LEN - 1] = 0;
	m->fields[n].length = length;
	m->fields[n].value = value;
	m->fields[n].spec = spec;
	if (n==0)
		m->fields[n].metadata_offset=32;
	else
		m->fields[n].metadata_offset=m->fields[n-1].metadata_offset+m->fields[n-1].length;
	m->fields_num++;
}

void match_fields_free(struct match_fields *m)
{
	free(m);
}

struct match_fields *match_fields_copy(struct match_fields *m)
{
	struct match_fields *mm = match_fields();
	memcpy(mm, m, sizeof(struct match_fields));
	return mm;
}


int match_fields_get_num(struct match_fields *mfs)
{
	return mfs->fields_num;
}

void match_fields_get_field(struct match_fields *mfs,int idx, char *name,int *offset,int *length)
{
	struct match_field mf=mfs->fields[idx];
	strncpy(name, mf.name, MATCH_FIELD_NAME_LEN);
//	name=mf.name;
	*offset = mf.metadata_offset;
	*length=mf.length;
}


void match_fields_generate_action(struct match_fields *mfs, struct header *now_spec,
			  struct action *a)
{
	int i;
	for (i=0;i<mfs->fields_num;i++){
		if (mfs->fields[i].spec==now_spec)
		{
			if(mfs->fields[i].metadata_type == 0)
				action_add_write_metadata_from_packet(a,mfs->fields[i].metadata_offset,mfs->fields[i].length,mfs->fields[i].offset);
			else
				action_add_write_metadata(a, mfs->fields[i].metadata_offset, mfs->fields[i].length, mfs->fields[i].value);
		}
	}
}



void collect_matchfields_get_complete_information(struct match_fields *collect_fields,struct match_fields *mfs)
{
	int i,j;
	for( i=0;i<collect_fields->fields_num;i++)
	{
		if(collect_fields->fields[i].metadata_type == 0)
		{
			printf("collect_fields[%d] %s:offset %d metadataoffset %d length %d\n",i,collect_fields->fields[i].name,
					collect_fields->fields[i].metadata_offset,
					collect_fields->fields[i].length,
					collect_fields->fields[i].offset
					);
		}
		else
		{
			printf("Curtis: collect_fields[%d] %s: metadataoffset: %d length: %d value: %d\n",i,collect_fields->fields[i].name,
					collect_fields->fields[i].metadata_offset,
					collect_fields->fields[i].length,
					value_to_8(collect_fields->fields[i].value)
					);
		}
		for (j=0;j<mfs->fields_num;j++)
			if(strcmp(collect_fields->fields[i].name,mfs->fields[j].name)==0){
				if(collect_fields->fields[i].metadata_type == 0)
				{
					collect_fields->fields[i].metadata_offset=mfs->fields[j].metadata_offset;
					collect_fields->fields[i].length=mfs->fields[j].length;
					collect_fields->fields[i].offset=mfs->fields[j].offset;
					printf("**Find collect_fields[%d] %s: metadataoffset %d length %d offset %d\n",i,collect_fields->fields[i].name,
							collect_fields->fields[i].metadata_offset,
							collect_fields->fields[i].length,
							collect_fields->fields[i].offset
							);
				}
				else
				{
					collect_fields->fields[i].metadata_offset=mfs->fields[j].metadata_offset;
					collect_fields->fields[i].length=mfs->fields[j].length;
					collect_fields->fields[i].value=mfs->fields[j].value;
					printf("Curtis FFFFFFFind: collect_fields[%d] %s: metadataoffset: %d length: %d value: %d\n",i,collect_fields->fields[i].name,
							collect_fields->fields[i].metadata_offset,
							collect_fields->fields[i].length,
							value_to_8(collect_fields->fields[i].value)
							);
				}
				break;
			}
		if (j==mfs->fields_num){
			printf ("Error:Can't find the write_metadata before read_metadata\n");
			assert(0);
		}
	}
}


/*
void match_fields_dump(struct match_fields *m, char *buf, int n)
{
	int i;
	int offset = 0;
	offset += snprintf(buf + offset, n - offset, "MATCH: ");
	for(i = 0; i < m->fields_num; i++)
		offset += snprintf(buf + offset,
				   n - offset,
				   "%s = (%02x %02x %02x %02x %02x %02x) ",
				   m->m[i].name,
				   m->m[i].value.v[0], m->m[i].value.v[1],
				   m->m[i].value.v[2], m->m[i].value.v[3],
				   m->m[i].value.v[4], m->m[i].value.v[5]);
}

#ifdef ENABLE_WEB
#include "core/packet_parser.h"
int match_dump_json(struct match *m, struct header *h, char *buf)
{
	int i, k;
	int offset, length;
	int pos = 0;

	for(i = 0; i < m->fields_num; i++) {
		if(strcmp(m->m[i].name, "in_port"))
			header_get_field(h, m->m[i].name, &offset, &length);
		else
			length = 8;
		length = (length + 7) / 8;
		pos += sprintf(buf+pos, "\"%s\":\"", m->m[i].name);
		for(k = 0; k < length; k++) {
			pos += sprintf(buf+pos,
				       "%02x",
				       m->m[i].value.v[k]);
		}
		pos += sprintf(buf+pos,
			       "\"%s",
			       i == m->fields_num - 1 ? "" : ",");
	}
	return pos;
}
#endif
*/
