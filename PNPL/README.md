#PNPL说明文档

this is a high-level language for simplifying  POF network programming.

在传统网络中，每个网络设备不但处理网络中的数据包，还运行着分布式的网络算法， 并共同决定着整个网络的行为。这种分布式的、数据和控制紧密耦合的体系结构难配置，易 出错，也使网络算法难以自由修改。SDN 是一种数据和控制分离的网络体系结构。在这种 结构中，网络设备运行在数据面，即只需要处理网络中的数据包。控制面，即网络算法，则 运行在一个逻辑上集中的控制器中。控制面和数据面通过一个标准的基于流表的协议来进行 通信。这样，我们只需要简单地对控制器上运行的网络算法进行配置或修改，就可以改变整 个网络的行为。 

Openflow 1.x是一套标准的数据面-控制面通信协议规范。然而，直接依照 Openflow 1.x 来写网络算法仍然是一件不方便且易出错的事。首先，Openflow 1.x 规定的协议比较底层， 使得程序员不能专注于算法本身，而是经常要处理控制面-数据面之间通信的细节。第二， Openflow 1.x 的数据面只能识别和处理固定几种网络协议的数据包。这限制了网络的可编 程能力，让一些网络算法不得不变通地实现甚至不能实现。 
Maple[3]是一个高层的 SDN控制器编程环境。它以 C等高级语言为基础，加上一些API， 使用户能够在较高抽象层次上对 SDN 进行编程，而不需要程序员关心如何操作流表等底层 的问题。 POF[1,2]是华为公司主导提出的新的数据面-控制面通信协议规范，是 Openflow 2.0 的 候选之一。它通过{offset, length}-对来避免让数据面依赖于具体的数据包协议，把定义具体 协议的工作完全交给了程序员。 

我们设计的 PNPL 是一个新的基于 POF 的 SDN 控制器 编程环境。它采用 Maple 的基本技术和 POF 提供的新能力来解决前述两个方面的问题，目 的是使程序员能够更简单可靠地编写网络算法。 

#安装使用
PNPL 运行于 Linux 环境下。安装之前请确保已安装 Python2.0 及以上版本和相关库。
步骤 1、安装相关支持

1.1 安装libev：
```
sudo apt-get install libev-dev
```
1.2 安装Django：	
```
sudo apt-get install python-pip
sudo pip install Django==1.8.7
```
1.3 安装gparser的依赖项
```
sudo apt-get install graphviz
```
1.4 在pnpl_metadata/pox/中，make时的依赖项：（注：只有使用PCTRL.git时才需要）
```
sudo apt-get install python-dev
```


步骤 2：安装、配置 PNPL
```
python setup.py
```
正确执行配置脚本，终端将显示如下结果：
 ``` 
[*]Completed setting the primary path. 
[*]Start compiling PNPL...
[*]Start setting PNPL... 
[*]Completed setting PNPL.
```

详细请参考doc文档下的说明

#文档说明

文件夹:
pnpl_origin        		PNPL高级编程环境原始版本
pnpl_for_java    		加入java_API
pnpl_metadata    		加入metadata的使用

gparser        			协议解析工具 将a.header文件解析为图片形式
mininet.tar.gz     		安装mininet时使用
pofswitch-1.4.0.015	    pofswitch源码
UI_old        			Django框架网站,用于PNPL工具的可视化，旧版，通过pox进行间接连接，即PNPL连接pox，pox连接底层网络
UI_new					Django框架网站,用于PNPL工具的可视化，新版，PNPL直接连接底层网络
PCTRL.git    			POX版本控制器,可直接作为控制器,加载PNPL作为模块
pnplfile        			网站版本PNPL生成的输入文件存放位置
doc        				相关文档
 
文件:
lib_install    			需要安装的库以及mininet安装和pofswithc安装
run.py        			运行网站模式的控制器
setup_origin.py    		编译并配置整个项目,将pnpl_origin编译为动态链接库以便UI调用;
setup_m.py				编译并配置整个项目,将pnpl_metadata编译为动态链接库以便UI调用;
setup_m_newUI.py		使用直连PNPL的方式，而不是通过pox间接连接。
setup_java.py			java版本的安装。

#安装mininet
cd mininet
./local-install
cd ..

#安装pofswitch
cd pofswitch-1.4.0.015
./configure
make
sudo make install
cd ..

this is Master branch.

