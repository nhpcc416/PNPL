var mainctl =mainctl ||{}
mainctl.share={
  showContent: function(url){
    $.get(url, null, function (msg) {
	    if (msg=="FinishButDontPrint"){
	  		alert("FinishButDontPrint")
		  	$("#log").empty();
		  }
  		else{
	  		$("#log").empty();
		  	$("#log").html(msg);
	  	}
	  });
  },

  beginPof : function(url){
	  $.get(url, null, function (msg) {
	  	if (msg=="FinishButDontPrint"){
	  		alert("Start Success!");
		  	var client = new SocketClient("127.0.0.1",8182,"chat");
		  	client.connect();
		  	client.onData  = function(text) {
		  		console.log(text);
			  	element=document.getElementById("log");
			  	if(element){
			  		if(element.value.length >100000) {
			  			element.value="";
			  		}
				  	element.value += (text + "\n");
				  	element.scrollTop = element.scrollHeight;
			  	}
			  }
		  	client.onRegist = function() {
		  		this.sendData("I am coming!");
		  	}
		  	
		  	<!-- begin the info socket-->
					info_client = new SocketClient("127.0.0.1",8183,"infochat");
					info_client.connect();

					info_client.onData  = function(text) {
						console.log("info:"+text);
						clinet_on_Message(text);
						<!--alert(text);-->
					}
		  }
	  	else{
	  		$("#log").empty();
	  		$("#log").html(msg);
	  	}
  	});
  }
};

function SocketClient(ip,port,query) {
	var _this = this;
	this.socket = '';
	this.uid = 0;
	this.sign = '';
	this.connect = function() {
		this.socket = new WebSocket('ws://'+ip+':'+port+'/'+query);
		this.socket.onopen = function() {
			_this.onOpen()
		}
		this.socket.onmessage = function(event) {
			data = event.data;
			data = data.split("<split>")
			_this.uid = data[0];
			_this.sign = data[1];
			text = data[2];
			if(text!='SETUID') {
				_this.onData(text);
			}
			else {
				_this.onRegist()
			}
		}
		this.socket.onclose = function(event) {
			_this.onClose();
		};
	}
	this.onRegist = function() {

	}
	this.onClose = function() {

	}

	this.onOpen = function() {

	}

	this.onData = function(text) {

	}

	this.sendData = function (text) {
		var data = this.uid+'<split>'+this.sign+'<split>'+text
		this.socket.send(data);
	}

	this.close = function() {
		this.socket.close();
	}
}

$(document).ready(function(){
	$("#showcompile").click(function(){
		var SpecNameToRun = $("#SpecNameToRun").val();
	  var FuncNameToRun = $("#FuncNameToRun").val();
	  $.get("/show_compile/",{'SpecNameToRun':SpecNameToRun, 'FuncNameToRun':FuncNameToRun}, function(ret){
	  	$("#ContentOfCompile").val(ret);
	  })
	  //alert("aaa");
  });
  $("#DeliveryAll").click(function(){
	  var SpecNameToRun = $("#SpecNameToRun").val();
	  var FuncNameToRun = $("#FuncNameToRun").val();
	  $.get("/copy_compile/",{'SpecNameToRun':SpecNameToRun, 'FuncNameToRun':FuncNameToRun}, function(ret){
	  	if(ret == "yes"){
	  		alert("copy success.");
	  	}
	  	else{
	  		alert("fail, no such file.");
	  	}
	  })
	  //alert("aaa");
  });
});