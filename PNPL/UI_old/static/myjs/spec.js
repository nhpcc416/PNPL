  var real_list_spec_open = new Array();
  var pic_index = 0;
  //var real_list = new Array();
  $(document).ready(function(){
    $("#addspec").click(function(){
      //var text = editor_spec.getValue();
    	//alert(text);
    	//editor_spec.getDoc().setValue('var msg = "Hi";');
    	var NameOfSpec = $("#NameOfSpec").val();
    	var temp = NameOfSpec.indexOf(".spec");
    	if(temp == NameOfSpec.length-5){
    		$.get("/add_spec/",{'NameOfSpec':NameOfSpec}, function(ret){
    			if(ret == "empty"){
    			  //$("#ContentOfSpec").val("");
    			  editor_spec.getDoc().setValue('');
    			  $("#SpecNameToRun").val(NameOfSpec);
    			  alert("Create Success!" + '\n' + "You can write and save the f function now!");
    			}
    			else{
    			  //$("#ContentOfSpec").val(ret);
    			  editor_spec.getDoc().setValue(ret);
    			  $("#SpecNameToRun").val(NameOfSpec);
    			  alert("File already exists!" + '\n' + "We read this file for you!");
    			}
    		})
    	}
      else{
    		alert("False!" + '\n' + "Please set the Suffix as .spec!");
    	}
    });
    $("#readspec").click(function(){
      var NameOfSpec = $("#NameOfSpec").val();
    	$.get("/read_spec/",{'NameOfSpec':NameOfSpec}, function(ret){
    		if(ret == "empty"){
    			alert("File not exists!" + '\n' + "You must create this file first!" + '\n' + "Try the add button!");
    		}
    		else{
    			//$("#ContentOfSpec").val(ret);
    			editor_spec.getDoc().setValue(ret);
    			$("#SpecNameToRun").val(NameOfSpec);
    		}
    	})
    });
    $("#savespec").click(function(){
      var NameOfSpec = $("#NameOfSpec").val();
    	//var ContentOfSpec = $("#ContentOfSpec").val();
    	var ContentOfSpec = editor_spec.getValue();
    	$.get("/save_spec/",{'NameOfSpec':NameOfSpec, 'ContentOfSpec':ContentOfSpec}, function(ret){
    	  if(ret == "no"){
    			alert("File not exists!" + '\n' + "You must create this file first!" + '\n' + "Try the add button!");
    		}
    		else{
    			$("#SpecNameToRun").val(NameOfSpec);
    			alert("save success!");
    		}
    	})
    });
    $("#showspec").click(function(){
      var NameOfSpec = $("#NameOfSpec").val();
    	$.get("/show_spec/",{'NameOfSpec':NameOfSpec}, function(ret){
        ret = eval('(' + ret + ')');
        var spec_name_list = ret.spec_name_list;
        var real_list = new Array();
        //real_list = [];
        real_list_spec_open = [];
        for(i = 0;i < spec_name_list.length; i++){
          var temp = spec_name_list[i].indexOf(".spec");
    	    if(temp == spec_name_list[i].length-5){
    			  j = real_list.push(spec_name_list[i]);
    			}
        }
        real_list_spec_open = real_list;
        var table=$("<table class=\"table table-bordered\">");
        $("#createtable_spec").html(table);
        var tr_header = $("<tr></tr>");
        tr_header.appendTo(table);
        var td_header=$("<td>"+"num"+"</td>"+"<td>"+"name"+"</td>"+"<td>"+"operation"+"</td>");
        td_header.appendTo(tr_header);
        for(var i=0;i<real_list.length;i++){
          var tr=$("<tr></tr>");
          tr.appendTo(table);
          num_of_spec=i+1;
          var td=$("<td>"+num_of_spec+"</td>"+"<td>"+real_list[i]+"</td>"+"<td>"+"<button type=\"button\" class=\"btn btn-default btn-xs\" onclick=\"List_Open_Spec(this)\">"+"<span class=\"glyphicon glyphicon-open\">"+"</span>"+"open"+"</button>"+"</td>");
          td.appendTo(tr);
        }
        $("#createtable_spec").append("</table>");
    		//alert(real_list.length+" files exists.");
    	})
    });
    $("#showpic").click(function(){
    	var NameOfSpec = $("#NameOfSpec").val();
    	$.get("/pic_spec/",{'NameOfSpec':NameOfSpec}, function(ret){
    		ret = eval('(' + ret + ')');
    		var pic_list = ret.pic_list;
    		if(pic_list[0] == "yes"){
    			//var image = $("<img src=\"static/test"+pic_index+".png\" style=\"max-width:100%;\" />");
    			
          //$("#ShowPicAndErr").html(image);
          ShowPicAndErr_p.style.display="none";
          ShowPicAndErr_pic.style.display="block";
          ShowPicAndErr_pic.src ="static/test"+pic_index+".png";
          pic_index++;
    			pic_index = pic_index % 2;
    			
    		}
    		else{
    			ShowPicAndErr_p.style.display="block";
    			ShowPicAndErr_pic.style.display="none";
    			$("#ShowPicAndErr_p").html(pic_list[1]);
    		}
    	})
    });
  });
  function List_Open_Spec(index){
    currentStep=$(index).parent().parent().find("td:first-child").html();
    //alert(real_list_spec_open[currentStep-1]);
    $("#NameOfSpec").val(real_list_spec_open[currentStep-1]);
    $("#readspec").click();
  }